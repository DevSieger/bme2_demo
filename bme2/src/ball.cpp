/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <bme2/ball.hpp>
#include <bme2/system.hpp>

namespace bme2 {

Ball::Ball(System& sys, const Ball_parameters& params){
    sys.add(*this,params);
}

Ball::~Ball() {
    if(sys_)
        sys_->remove(*this);
}

Ball::Ball(Ball&& other) noexcept {
    if(other.sys_) System::swap(*this,other);
}

void Ball::swap(Ball& other){
    System::swap(*this, other);
}

bool Ball::apply(Force_ptr force, const Vec3 point, bool local) const {
    return sys_->apply(std::move(force),*this,point,local);
}

void Ball::detach(Force& f, const Vec3 point, bool local) const{
    sys_->detach(f,*this,point,local);
}

void Ball::reset()
{
    sys_    = nullptr;
    d_      = dummy_params();
}

const Ball_parameters* Ball::dummy_params() noexcept
{
    // this replaces nullptr value for Ball::d_ to simplify getters.
    // if a Ball object is "outside" of a System, it has these parameters
    static const Ball_parameters dummy_params_value;
    return &dummy_params_value;
}

} // namespace bme2
