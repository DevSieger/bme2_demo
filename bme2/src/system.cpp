/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <bme2/system.hpp>
#include <bme2/ball.hpp>
#include <bme2/force.hpp>
#include <bme2/static_object.hpp>
#include <algorithm>
#include <glm/glm.hpp>

namespace bme2 {

namespace {

// Force_entry is private, no checking :(
template<typename ... Container>
inline void detach_all(Ball& obj, Container&... c){
    using std::cbegin, std::cend;
    ( std::for_each(cbegin(c), cend(c),[ptr=&obj](auto& entry){ entry.force->on_detach(*ptr,entry.point,entry.local);}), ...);
}
template<typename ... Container>
inline void apply_all(Ball& obj, Container& ... c){
    using std::cbegin, std::cend;
    ( std::for_each(cbegin(c), cend(c),[ptr=&obj](auto& entry){ entry.force->on_attach(*ptr,entry.point,entry.local);}), ...);
}

}

System::System(System&& other) noexcept:
    obj_(std::move(other.obj_)),
    local_f_(std::move(other.local_f_)),
    global_f_(std::move(other.global_f_)),
    hdata_(std::move(other.hdata_))
{
    for(auto& p: obj_){
        p->sys_ = this;
        // p->d_ remains valid
    }

    // is there any guaranty in pre c++17 standard that an object of type std::vector
    // is empty after its content has been moved?
    // the following lines may be redundant
    other.obj_.clear();
    other.local_f_.clear();
    other.global_f_.clear();
    other.hdata_.clear();
}

System::~System()
{
    for(std::size_t ix = 0; ix < obj_.size(); ++ix){
        clean_up(ix);
    }
}

void System::clean_up(System::Ix ix)
{
    assert( ix < obj_.size() );
    detach_all( *obj_[ix], local_f_[ix], global_f_ );
    local_f_[ix].clear();
    obj_[ix]->reset();
}

void System::add(Ball& obj, const Ball_parameters& params)
{
    if( obj.sys_ ){
        obj.sys_->remove(obj);
    }
    obj.sys_ = this;

    // todo: handle exceptions
    auto cap = hdata_.capacity();
    obj_.push_back(&obj);
    local_f_.push_back({});
    hdata_.push_back(params);

    if( hdata_.size() > cap ){
        for(Ix i = 0, last = hdata_.size(); i < last; ++i){
            bind(*obj_[i] , i);
        }
    } else {
        bind(obj,hdata_.size()-1);
    }

    apply_all(obj, global_f_);
}

void System::remove(Ball& obj)
{
    assert(obj.sys_ == this);
    Ix ix = get_ix(obj);
    assert(ix < obj_.size() && &obj == obj_[ix]);

    clean_up(ix);

    if(ix != obj_.size() - 1){
        hdata_[ix]      = std::move(hdata_.back());
        obj_[ix]        = std::move(obj_.back());
        local_f_[ix]    = std::move(local_f_.back());
        bind(*obj_[ix], ix);
    }

    hdata_.pop_back();
    obj_.pop_back();
    local_f_.pop_back();
}

void System::add(Object_ptr object)
{
    st_obj_.push_back(std::move(object));
}

bool System::remove(Static_object& object)
{
    auto it = std::find_if(st_obj_.cbegin(), st_obj_.cend(), [&object](const Object_ptr& optr) { return  optr.get() == &object;} );
    if( it != st_obj_.cend()){
        st_obj_.erase(it);
        return true;
    }
    return false;
}

bool System::apply(Force_ptr local_force, const Ball& obj, const Vec3 point, bool local)
{
    if(!local_force) return false;
    assert(obj.sys_ == this);
    Ix ix = get_ix(obj);
    if(local_force->on_attach(obj,point,local)){
        local_f_[ix].push_front(Force_entry{std::move(local_force),point,local});
        return true;
    }
    return false;
}

bool System::detach(Force& local_force, const Ball& obj, const Vec3 point, bool local)
{
    assert(obj.sys_ == this);
    Ix ix = get_ix(obj);
    auto& c = local_f_[ix];
    for(auto it = c.before_begin(), prev = it++, lst = c.end(); it!=lst; prev = it++ ){
        if( it->force.get() == &local_force && it->point == point && it->local == local ){
            it->force->on_detach(obj,point,local);
            c.erase_after(prev);
            return true;
        };
    }
    return false;
}

void System::apply(Force_ptr global_force, const Vec3 point, bool local)
{
    if(!global_force) return;
    global_f_.push_back(Force_entry{std::move(global_force),point,local});
    auto& entry = global_f_.back();
    for(auto& p: obj_){
        entry.force->on_attach(*p,entry.point,entry.local);
    }
}

bool System::detach(Force& global_force, const Vec3 point, bool local)
{
    auto pred   = [&global_force, &point, &local](const Force_entry& entry)->bool {
        return  entry.force.get()   == &global_force &&
                entry.point         == point &&
                entry.local         == local;
    };

    auto it = std::find_if(global_f_.begin(), global_f_.end(), pred);
    if( it != global_f_.end() ){
        if( global_f_.end() - it > 1){
            *it = std::move(global_f_.back());
        }
        global_f_.pop_back();
        return true;
    }
    return false;
}

void System::swap(Ball& lhv, Ball& rhv) noexcept
{
    if( lhv.sys_ == nullptr && rhv.sys_ == nullptr) return;

    using std::swap;

    if( lhv.sys_ )  detach_all( lhv, lhv.sys_->local_f_[get_ix(lhv)], lhv.sys_->global_f_);
    if( rhv.sys_ )  detach_all( rhv, rhv.sys_->local_f_[get_ix(rhv)], rhv.sys_->global_f_);

    if( lhv.sys_ != nullptr && rhv.sys_ != nullptr ){
        System& ls  = *lhv.sys_;
        System& rs  = *rhv.sys_;
        Ix lix      = get_ix(lhv);
        Ix rix      = get_ix(rhv);
        swap( ls.obj_[lix], rs.obj_[rix] );
    }else{
        Ball& from  = (lhv.sys_ != nullptr)? lhv : rhv;
        Ball& to    = (lhv.sys_ != nullptr)? rhv : lhv;
        from.sys_->obj_[get_ix(from)] = &to;
    }

    swap( lhv.d_,   rhv.d_ );
    swap( lhv.sys_, rhv.sys_ );

    if( lhv.sys_ )  apply_all( lhv, lhv.sys_->local_f_[get_ix(lhv)], lhv.sys_->global_f_);
    if( rhv.sys_ )  apply_all( rhv, rhv.sys_->local_f_[get_ix(rhv)], rhv.sys_->global_f_);
}

System::Ix System::get_ix(const Ball& ball)
{
    assert(ball.sys_);
    return ball.d_ - ball.sys_->hdata_.data();
}

void System::bind(Ball& ball, System::Ix data_ix)
{
    assert(ball.sys_ == this);
    assert(data_ix < hdata_.size());
    ball.d_ = hdata_.data() + data_ix;
}

void System::iterate(Time dt)
{
    // too difficult and complex method, iterative development

    Time dtt    = dt*dt;

    for(Ix ix = 0; ix < obj_.size(); ++ix){
        auto& d             = hdata_[ix];
        const Ball& ball    = *obj_[ix];
        Force_value fv{};
        for(auto& entry: global_f_){
            fv += entry.force->get(ball,entry.point, entry.local);
        }
        for(auto& entry: local_f_[ix]){
            fv += entry.force->get(ball,entry.point, entry.local);
        }

        // "balistic integrator"
        d.pos  += 0.5 * dtt* fv.force / d.mass + d.vel * dt;
        d.vel  += fv.force / d.mass * dt;

        // Euler's method
        const Quat y    = d.rot + dt * (0.5*(Quat(0, d.w)*d.rot));
        const Vec3 nw   = d.w + (dt / (0.4*d.mass*d.radius*d.radius)) * fv.moment;

        d.rot  += dt * 0.25 * ( Quat(0, d.w) * d.rot + Quat(0, nw)*y);
        d.w     = nw;
        auto qq = glm::dot(d.rot,d.rot);
        if( 0.995 < qq || qq < 1.005 ){
            if(qq > 0){
                d.rot   = glm::normalize(d.rot);
            } else {
                d.rot   = Quat(1,0,0,0);
            }
        }
    }

    using glm::dot, glm::cross, glm::length;

    // TODO: move this to cfg
    /*
     * threshold values for a hack that reduces infinite vibrations:
     * if objects is closer than pos_threshold and they are aproaching each other with a velocity
     * lesser than vel_threshold then "recovery coefficient" (k) is set to zero.
     */
    const double vel_threshold = 0.00001, pos_threshold = 0.0000001;

    // brute force CD
    for(Ix i = 0; i < obj_.size(); ++i){
        for(Ix j = i+1; j < obj_.size(); ++j){
            auto& d1    = hdata_[i];
            auto& d2    = hdata_[j];
            Vec3 tmp    = d2.pos - d1.pos;
            const auto sqd      = dot(tmp,tmp);
            if( sqd <= (d1.radius + d2.radius) * (d1.radius + d2.radius) ){
                const double epsilon = glm::epsilon<double>();

                const auto [k0,fs,fk,fw]
                        = mat_ ? mat_->collision_parameters(*obj_[i], *obj_[j]) : Material_interaction::default_parameters();

                const double dist   = glm::sqrt(sqd);
                const Vec3 n    = tmp / dist;
                const auto dvn  = dot(d2.vel - d1.vel, n);
                if( dvn > 0) continue;

                // a hack that reduces infinite vibrations
                const double k  = ( -dvn < vel_threshold && dist - (d1.radius + d2.radius) < pos_threshold && k0 < 1)? 0 : k0;

                const auto sn   = (1+k)*d1.mass*d2.mass*dvn / (d1.mass + d2.mass);
                const Vec3 vk   = d2.vel - d1.vel - dvn*n - cross(d2.w*d2.radius + d1.w*d1.radius, n);

                // Terms may be wrong
                // TODO: fix terms in comments
                // an impulse of a slide friction
                Vec3 ss;
                const auto vksl = dot(vk,vk);
                if(vksl > 0){
                    ss = glm::min( -fs*sn * glm::inversesqrt(vksl), (0.4/1.4)*d1.mass*d2.mass/(d1.mass+d2.mass) ) * vk;
                }else{
                    ss  = Vec3(0,0,0);
                }

                const double tt1 = 0.4*d1.mass*d1.radius*d1.radius;
                const double tt2 = 0.4*d2.mass*d2.radius*d2.radius;
                const double tmp = (tt1 * tt2) / ( tt1 + tt2);

                // a moment of a whirl friction
                Vec3 mwh;
                const auto dwn  = dot(d2.w - d1.w, n);
                if( glm::epsilonNotEqual(dwn, 0.0, epsilon) ){
                    mwh = glm::min( -fw * sn, tmp * glm::abs(dwn) )*glm::sign(dwn)*n;
                } else {
                    mwh = Vec3(0,0,0);
                }

                // a moment of a roll friction
                Vec3 mr;
                const Vec3 b    = cross(n,ss);
                const Vec3 dwk  = (d2.w - d1.w) - dwn * n + b*(d2.radius/tt2 - d1.radius/tt1);

                const auto dwksl    = dot(dwk,dwk);
                if(dwksl > 0){
                    mr = glm::min( -fk*sn * glm::inversesqrt(dwksl), tmp) * dwk;
                }else{
                    mr  = Vec3(0,0,0);
                }

                const Vec3 impulse  = ss + sn*n;
                const Vec3 moment1  = d1.radius*b + mr + mwh;
                const Vec3 moment2  = d2.radius*b - mr - mwh;

                d1.vel += impulse/d1.mass;
                d2.vel -= impulse/d2.mass;
                d1.w   += moment1/tt1;
                d2.w   += moment2/tt2;

                double mc   =   d2.mass/(d2.mass+d1.mass);
                d1.pos     -= mc*(d1.radius+d2.radius - dist) * n;
                d2.pos     += (1-mc)*(d1.radius+d2.radius - dist) * n;

                if(collision_handler_) collision_handler_->on_collision(*obj_[i], *obj_[j], impulse, moment1, moment2);
            }
        }
    }

    for(Ix i = 0; i < obj_.size(); ++i){
        auto& bd = hdata_[i];
        for(auto& st: st_obj_){
            Vec3 corr{}, n{};
            if( st->check_collision(bd.pos,bd.radius,&n,&corr) ){

                auto vn = dot(bd.vel,n);

                if(vn > 0) continue;

                const double epsilon = glm::epsilon<double>();

                const auto [k0,fs,fk,fw]
                        = mat_ ? mat_->collision_parameters(*obj_[i], *st) : Material_interaction::default_parameters();

                // a hack that reduces infinite vibrations
                const double k  = ( -vn < vel_threshold && dot(corr,corr) < pos_threshold*pos_threshold && k0 < 1)? 0 : k0;

                auto sn = -(1+k)*bd.mass*vn;

                Vec3 ss;
                Vec3 vk = - (bd.vel -  vn*n - bd.radius * cross (bd.w, n) );
                const auto vksl = dot(vk,vk);

                const double mp2 = 0.4*bd.mass * bd.radius*bd.radius;

                if(vksl > 0){
                    ss = glm::min( fs*sn * glm::inversesqrt(vksl), (0.4/1.4)*bd.mass ) * vk;
                }else{
                    ss  = Vec3(0,0,0);
                }

                const Vec3 mom_ss   = bd.radius * cross(ss,n);// == bd.radius * cross(-n,ss)
                const auto dwn      = dot(bd.w, n);
                const Vec3 dwk  = - ( bd.w - dwn * n + mom_ss/ mp2 );

                Vec3 mom            = mom_ss;

                if( glm::epsilonNotEqual(dwn, 0.0, epsilon) ){
                    mom    += glm::min( fw * sn, mp2 * glm::abs(dwn) )*glm::sign(-dwn)*n;
                }

                const auto dwksl    = dot(dwk,dwk);
                if(dwksl > 0){
                    mom += glm::min( fk*sn * glm::inversesqrt(dwksl), mp2 ) * dwk;
                }

                const Vec3 impulse  = ss + sn*n;

                bd.vel += impulse / bd.mass;
                bd.w   += mom / mp2;
                bd.pos += corr;

                if(collision_handler_) collision_handler_->on_collision(*obj_[i], *st, impulse, mom);
            }
        }
    }
}

} // namespace bme2
