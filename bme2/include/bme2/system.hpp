/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <forward_list>
#include <memory>
#include <vector>
#include <utility>
#include "common.hpp"

namespace bme2 {

// TODO: is a term "elasticity" correct?
/// Class that provides an information about elasticity and friction of objects.
class Material_interaction{
public:
    struct Collision_parameters{
        double recovery_coefficient;
        double slide_friction;
        double rolling_friction;
        double whirling_friction;
    };

    // this is used in the case of unspecified Material_interaction
    static constexpr Collision_parameters default_parameters() { return Collision_parameters{0.8, 0.5, 0.05, 0.01}; }

    virtual Collision_parameters collision_parameters(const Ball& b1, const Ball& b2) = 0;
    virtual Collision_parameters collision_parameters(const Ball& ball, const Static_object& st_obj) = 0;
    virtual ~Material_interaction() = default;
};

using Material_interaction_ptr = std::shared_ptr<Material_interaction>;

class Collision_handler{
public:
    /*!
     * \brief Serves as a callback to notify about collision of two Ball objects.
     * \param b1        the frist Ball object
     * \param b2        the second Ball object
     * \param impulse_1 the impact impulse that is received by the first object, the second object receives an impulse equal to -impulse1
     * \param moment_1  the moment of the impulse received by the first object, relative to the center of mass of the object
     * \param moment_2  the moment of the impulse received by the second object, relative to the center of mass of the object
     */
    virtual void on_collision(Ball& b1, Ball& b2, const Vec3& impulse_1, const Vec3& moment_1, const Vec3& moment_2) = 0;
    virtual void on_collision(Ball& b1, Static_object& st, const Vec3& impulse, const Vec3& moment) = 0;
    virtual ~Collision_handler() = default;
};

using Collision_handler_ptr = std::shared_ptr<Collision_handler>;

class System{
public:
    System() = default;
    System(System&& other) noexcept;
    ~System();

    void add(Ball& obj, const Ball_parameters& params);
    void remove(Ball& obj);

    void add(Object_ptr object);
    bool remove(Static_object& object);

    bool apply(Force_ptr local_force, const Ball& obj, const Vec3 point, bool local);
    bool detach(Force& local_force, const Ball& obj, const Vec3 point, bool local);

    void apply(Force_ptr global_force, const Vec3 point, bool local);
    /// Detaches one force that statisfies the given parameters, returns false if there is no such a force
    bool detach(Force& global_force, const Vec3 point, bool local);

    void iterate(Time dt);

    template<typename Interaction, typename ... Ps>
    std::enable_if_t<std::is_base_of<Material_interaction,Interaction>::value> make_material_interaction(Ps&& ... ps){
        material_interaction(std::make_shared<Interaction>(std::forward<Ps>(ps)...));
    }

    const Material_interaction_ptr& material_interaction() const {
        return mat_;
    }
    void material_interaction(Material_interaction_ptr p_material_interaction){
        mat_ = std::move(p_material_interaction);
    }

    template<typename Handler, typename ... Ps>
    std::enable_if_t<std::is_base_of<Collision_handler,Handler>::value> make_collision_handler(Ps&& ... ps){
        material_interaction(std::make_shared<Handler>(std::forward<Ps>(ps)...));
    }
    const Collision_handler_ptr& collision_handler() const {
        return collision_handler_;
    }
    void material_interaction(Collision_handler_ptr p_collision_handler){
        collision_handler_ = std::move(p_collision_handler);
    }

    static void swap(Ball& lhv, Ball& rhv) noexcept;

private:
    struct Force_entry{
        Force_ptr   force;
        Vec3        point;
        bool        local;
    };

    using Forces    = std::forward_list<Force_entry>;
    using Ix        = std::size_t;

    static Ix get_ix(const Ball& ball);

    void bind(Ball& ball, Ix data_ix);

    // prepares the object with the given index for removal
    void clean_up(Ix ix);

    std::vector<Ball*>              obj_;
    // forces is a part of the system, not an object
    // lists of "local" forces influencing to single object.
    std::vector<Forces>             local_f_;
    // global forces
    std::vector<Force_entry>        global_f_;
    // actual data
    std::vector<Ball_parameters>    hdata_;
    // static objects
    std::vector<Object_ptr>         st_obj_;

    Material_interaction_ptr        mat_;
    Collision_handler_ptr           collision_handler_;
};

} // namespace bme2
