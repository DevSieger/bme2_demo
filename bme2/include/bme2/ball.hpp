/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include "common.hpp"

namespace bme2 {

/// This class is a glue between System and a user code.
class Ball{
public:
    Ball (System& sys, const Ball_parameters& params);

    virtual ~Ball();

    Ball(Ball&& other) noexcept;

    void swap(Ball& other);

    Vec3    position()      const { return d_->pos; }
    Quat    rotation()      const { return d_->rot;}
    Mass    mass()          const { return d_->mass;}
    Vec3    velocity()      const { return d_->vel;}
    Dist    radius()        const { return d_->radius;}
    Vec3    angular_rate()  const { return d_->w;}
    System* system()        const { return sys_;}

    bool    apply(Force_ptr force, const Vec3 point, bool local) const;
    void    detach(Force& f, const Vec3 point, bool local) const;

    friend class System;
private:
    void reset();
    static const Ball_parameters* dummy_params() noexcept;
    const Ball_parameters*  d_      {dummy_params()};
    System*                 sys_    {nullptr};
};

} // namespace bme2
