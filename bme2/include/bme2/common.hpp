/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>
#include <memory>

namespace bme2 {

using Vec3  = glm::dvec3;
using Quat  = glm::dquat;

/// Mass, in kg
using Mass  = double;
/// Distance, in meters
using Dist  = double;
/// Time interval, in  seconds
using Time  = double;

class Ball;
class System;
class Force;
class Static_object;

using Force_ptr     = std::shared_ptr<Force>;
using Object_ptr    = std::shared_ptr<Static_object>;

struct Ball_parameters{
    /// Rotation
    Quat    rot     {1,0,0,0};

    /// Position
    Vec3    pos     {0,0,0};

    /// Mass
    Mass    mass    {1};

    /// Velocity
    Vec3    vel     {0,0,0};

    /// Radius
    Dist    radius  {1};

    /// Angular velocity
    Vec3    w       {0,0,0};
};

} // namespace bme2
