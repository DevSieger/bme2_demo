/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include "common.hpp"

namespace bme2 {

struct Force_value{
    Vec3 force;
    Vec3 moment;

    Force_value& operator += (const Force_value& rhv){
        force  += rhv.force;
        moment += rhv.moment;
        return *this;
    }
};

class Force{
public:
    virtual Force_value get (const Ball&, const Vec3 point, bool local) const = 0;
    virtual bool on_attach  (const Ball&, const Vec3 point, bool local) { return true;}
    virtual void on_detach  (const Ball&, const Vec3 point, bool local) {}
    // note, this destructor disables implicitly-declared move constructor
    virtual ~Force() = default;
};

} // namespace bme
