/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430

#ifndef Z_THRESHOLD
#define Z_THRESHOLD -0.1
#endif

#ifndef RADIUS_TUNE_FACTOR
#define RADIUS_TUNE_FACTOR 1.5
#endif

#ifndef MAX_SV_LENGTH_HACK
#define MAX_SV_LENGTH_HACK 256
#endif

uniform uint ls_ix = 0;

layout(location=0) in vec3  pos;
layout(location=1) in vec3  orig;
layout(location=2) in float radius;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

// the view space ball origin
flat out vec3   vs_orig;
flat out float  vs_ball_radius;
flat out float  vs_ls_radius;
flat out vec3   vs_ls_orig;
flat out vec3   vs_intensity;

mat3x3 get_basis(in vec3 dir){
    vec3 n  = vec3(0,0,1);
    float d = dot(n,dir);

    if(abs(d)>0.9999){
        n   = vec3(0,1,0);
        d   = dot(n,dir);
    }

    n       = normalize(n - d*dir);
    return mat3x3(n,cross(dir,n),dir);
}

float get_vs_z(float depth){
    const float e=-1;
    return proj[3][2]/(e*depth-proj[2][2]);
}

void main(void)
{
    vs_intensity            = pls[ls_ix].illumination;
    const float range       = get_pls_range(vs_intensity,pls[ls_ix].attenuation_p, get_pls_cut_off());

    vec3    dir             = orig - pls[ls_ix].position;
    float   dist            = length(dir);
            dir            /= dist;
    const   float   d2      = dist * ( radius / ( pls[ls_ix].radius + radius));
    const   float   h0      = sqrt(d2*d2 - radius*radius) * radius / d2;
    const   float   tb      = sqrt(radius*radius - h0*h0);
    const   mat3x3  b       = get_basis(dir);
    const   float   tga12   = h0/(d2-tb);

    // a length of the shadow volume
    float   t       = clamp(range-dist+tb,0,MAX_SV_LENGTH_HACK);

    // this means that the shadow volume above the ground for any t.
    const   float   tmp1    = dir.z+b[0].z*tga12*RADIUS_TUNE_FACTOR;
    if(tmp1 != 0) {
        const float z_optimized = (Z_THRESHOLD - orig.z - b[0].z*h0*RADIUS_TUNE_FACTOR + dir.z*tb)/tmp1;
        if(z_optimized > 0 ){
            t   = min(t,z_optimized);
        }
    }

    const   float   h1      = tga12*t+h0;
    const   float   h       = mix(h0,h1,pos.z);
    const   vec3    world_p = b*vec3( pos.xy * h * RADIUS_TUNE_FACTOR, pos.z*t - tb)+orig;

    vs_ball_radius  = radius;
    vs_ls_radius    = pls[ls_ix].radius;
    vs_orig         = (cam*vec4(orig,1)).xyz;
    vs_ls_orig      = (cam*vec4(pls[ls_ix].position,1)).xyz;

    gl_Position     = proj*cam*vec4(world_p,1 );
}
