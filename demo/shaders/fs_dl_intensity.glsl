/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430 core

// lighting_h.glsl is includ

uniform uint first_ls   = 0;
uniform uint last_ls    = 0;

out vec3 intensity;
uniform sampler2D shadow_tx;

void main(void)
{
    const ivec2 txc = ivec2(gl_FragCoord.xy);
    const vec4 shdw = texelFetch(shadow_tx,txc,0);

    if( all(equal(shdw,vec4(0)))) discard;

    LC_ctx ctx;
    load_context(ctx);

    const uint last = last_ls - first_ls;
    intensity = vec3(0);
    for(uint ix = 0; ix < last; ++ix){
        const uint ls_ix = first_ls + ix;
        intensity += shdw[ix] * intensity_modifier(ctx, dls[ls_ix].dir) * dls[ls_ix].intensity;
    }
}
