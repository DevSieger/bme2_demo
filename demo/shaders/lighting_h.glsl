/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430 core

// usage: header-like maner to avoid code dublication

#ifndef EPSILON
#define EPSILON 0.0000001
#endif

#ifndef PI
#define PI 3.141592653589793238
#endif

vec4    load_albedo_shininess(ivec2 coord);
vec3    load_normal(ivec2 coord);
vec3    load_position(ivec2 coord);
vec3    load_specular_color(ivec2 coord);

// light caculation context
struct LC_ctx{
    vec3 albedo;
    vec3 spec;
    float shininess;
    vec3 normal;
    vec3 nview_dir;
};

void load_context(out LC_ctx ctx){
    ivec2 txc       = ivec2(gl_FragCoord.xy);
    vec4 tmp        = load_albedo_shininess(txc);
    ctx.albedo      = tmp.rgb;
    ctx.spec        = load_specular_color(txc);
    ctx.shininess   = tmp.a;
    ctx.normal      = load_normal(txc);
    ctx.nview_dir   = -normalize(load_position(txc));
}

vec3 intensity_modifier(in LC_ctx ctx, in vec3 ld){
    float c = -dot(ctx.normal, ld);
    if(c < 0 ) return vec3(0,0,0);
    float a = min(c,1);
    float b = clamp(dot(ctx.nview_dir, reflect(ld,ctx.normal)),0,1);
    return (a * ctx.albedo.rgb + pow(b, ctx.shininess)* ctx.spec);
}

float get_shadow_factor(float ball_angle,float ls_angle, float btw_angle){
    float norm_factor   = max(ls_angle,EPSILON);
    float a = 1;
    float b = btw_angle / norm_factor;
    float c = ball_angle / norm_factor;

    if ( b >= a + c) return 0;
    if ( a >= b + c) return c*c/(a*a);
    if ( c >= b + a) return 1;

    // magic people, voodoo people!

    float aa    = a*a;
    float bb    = b*b;
    float cc    = c*c;

    int cs = 0;
    if( c > a && cc > bb + aa ) cs = 1;
    if( a > c && aa > bb + cc ) cs = 2;

    float d     = abs(aa + bb - cc) *0.5 / max(b,EPSILON);
    float h     = sqrt(aa - d*d);
    float piaa  = PI*aa;
    float d2    = (cs == 1)? b + d: abs(d-b);

    float ta    = asin(clamp(h/max(a,EPSILON),-1,1))*aa - h*d;
    if( cs == 1) ta = piaa - ta;

    float tc    = asin(clamp(h/max(c,EPSILON),-1,1))*cc - h*d2;
    if( cs == 2) tc = PI*cc - tc;
    return (ta + tc)/max(piaa,EPSILON);
};

float attenuation(in float dist, in vec3 intensity, in vec3 att, in float threshold){
    const vec3 luma_w   = vec3(0.2126,0.7152,0.0722);
    float   luminance   = dot(intensity, luma_w);
    float a = 1/(att.x + att.y*dist + att.z*dist*dist + EPSILON);
    if(luminance > threshold){
        a   = (luminance * a - threshold) / ( luminance - threshold);
    }else{
        a   = 0;
    }
    return a;
}
