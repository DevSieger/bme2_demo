/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430 core
// cfg_ubo.glsl is included

#ifndef EPSILON
#define EPSILON 0.0000001
#endif

out vec3 color;
in vec2 coord;

vec3    load_intensity(ivec2 coord);
float   load_lum_gmean();

vec3 RGB_to_xyY(vec3 rgb){
    const mat3x3 to_XYZ = mat3x3 (
        0.4124,    0.2126, 0.0193,
        0.3576,    0.7152, 0.1192,
        0.1805,    0.0722, 0.9502
    );
    vec3 XYZ    = to_XYZ * rgb;
    return  vec3( XYZ.xy/ ( dot(XYZ,vec3(1)) + EPSILON ),XYZ.y);
}

vec3 xyY_to_RGB(vec3 xyY){
    vec3 XYZ;
    XYZ.y       = xyY.z;
    XYZ.xz      = (xyY.z / (xyY.y + EPSILON) ) * vec2 (xyY.x, 1-xyY.x-xyY.y);

    const mat3x3 to_RGB = mat3x3 (
        3.2406,     -0.9689,    0.0557,
        -1.5372,    1.8758,     -0.2040,
        -0.4986,    0.0415,     1.0570
    );

    return  to_RGB * XYZ;
}

void main(void)
{
    ivec2 txc   = ivec2(gl_FragCoord.xy);  
    vec3 illu_xyY   =   RGB_to_xyY( load_intensity(txc));

    // the "Reinhard at al." global tone mapping operator.
    float average   =   load_lum_gmean();

    float key       =   tm_key_b *log(1 + tm_key_a * average) + tm_key_c;

    float slumi     =   key * illu_xyY.z / average;
    float white     =   burn_out_ratio * average;
    illu_xyY.z      =   slumi / (1 + slumi) * ( 1 + slumi / (white * white) );
    illu_xyY.z      =   clamp(illu_xyY.z,0,1);

    color           = xyY_to_RGB(illu_xyY);
}
