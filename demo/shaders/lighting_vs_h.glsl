/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430 core

float   load_lum_gmean();

// requires ubu.glsl
float get_pls_cut_off(){
    return load_lum_gmean() * pl_cut_off;
}

float get_pls_range(in vec3 intensity, in vec3 attenuation, in float cut_off){
    const vec3  luma_w      = {0.2126,0.7152,0.0722};
    const float rl          = dot(luma_w,intensity) / cut_off;

    // z*r*r + y*r + x - rl = 0
    // where r is the range to find
    // x,y,z are components of attenuation
    const float b           = 0.5 * attenuation.y/attenuation.z;
    const float d           = b*b - (attenuation.x - rl) / attenuation.z;
    // if d < 0 then pl_cut_off threshold cuts this light source entirely.
    return (d>=0)? -b + sqrt(d) : 0;
}
