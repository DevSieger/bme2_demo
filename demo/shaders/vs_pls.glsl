/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430

layout(location=0) in vec3  rpos;
layout(location=1) in vec3  orig;
layout(location=2) in float radius;
layout(location=3) in vec3  intensity;
layout(location=4) in vec3  attenuation;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

flat out vec3   vs_orig;
flat out vec3   vs_intensity;
flat out vec3   vs_attenuation;
flat out float  vs_cut_off;

#ifndef RADIUS_TUNE_FACTOR
#define RADIUS_TUNE_FACTOR 1.5
#endif

void main(void)
{
    // the sphere origin
    vs_orig         = (cam * vec4(orig,1)).xyz;
    vs_intensity    = intensity;
    vs_attenuation  = attenuation;
    vs_cut_off      = get_pls_cut_off();

    float range = get_pls_range(intensity, attenuation, vs_cut_off);

    // we need a larger ico sphere to cover a real sphere
    float g_rad = range * RADIUS_TUNE_FACTOR;

    // no need to rotate the mesh
    gl_Position = proj*vec4(g_rad*rpos + vs_orig,1);
    // remember: glEnable(GL_DEPTH_CLAMP);
}
