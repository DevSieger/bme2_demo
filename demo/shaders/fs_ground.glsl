/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430 core

uniform sampler2D ground_albedo_tx;
uniform sampler2D ground_normal_tx;
uniform sampler2D ground_spec_tx;

layout( location = 0 ) out vec4 albedo;
layout( location = 1 ) out vec2 normal;
layout( location = 2 ) out vec3 specular;

in vec3 vs_pos;
in mat3x3 vs_ts;

uniform float spot_radius   = 32;
uniform float ceil_scale    = 4;

vec2    decode_normal(vec3 n);

void main(void)
{
    if(dot(vs_pos,vs_pos) > spot_radius*spot_radius){
        discard;
    };
    vec2 crd    = vs_pos.xy / ceil_scale;
    albedo      = vec4(texture(ground_albedo_tx, crd).rgb,0);
    vec3 n_tx   = normalize(texture(ground_normal_tx, crd).rgb*2-vec3(1));
    normal      = decode_normal(vs_ts*n_tx);
    specular    = texture(ground_spec_tx,crd).rgb;
}
