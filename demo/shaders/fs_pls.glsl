/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430 core

// lighting_h.glsl is included

uniform sampler2D shadow_tx;

flat in vec3    vs_orig;
flat in vec3    vs_intensity;
flat in vec3    vs_attenuation;
flat in float   vs_cut_off;

out vec3 intensity;

uniform uint ls_ix = 0;

vec3    load_position(ivec2 coord);

void main(void)
{
    ivec2   txc     = ivec2(gl_FragCoord.xy);
    vec3    fpos    = load_position(txc);
    float   shdw    = texelFetch(shadow_tx,txc,0)[ls_ix%4];
    LC_ctx ctx;
    load_context(ctx);

    float   dist        = distance(fpos,vs_orig);
    float   att         = attenuation(dist, vs_intensity, vs_attenuation, vs_cut_off);
    vec3    ld          = (fpos - vs_orig)/dist;

    intensity = shdw * att * vs_intensity * intensity_modifier(ctx, ld);
}
