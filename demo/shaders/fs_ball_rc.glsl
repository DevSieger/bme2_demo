/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430 core

uniform sampler2D albedo_tx;

// note: _inverted_ rotation of the ball
flat in vec4    vs_rot;
flat in float   vs_radius;
flat in vec3    vs_orig;
flat in vec3    vs_color;

in vec4         vs_pos;

vec3 rotate(in vec4 quat, in vec3 v){
    return v+ 2 * cross( quat.xyz, (cross(quat.xyz,v)+quat.w*v));
}

layout(depth_greater) out float gl_FragDepth;

layout( location = 0 ) out vec4 albedo;
layout( location = 1 ) out vec2 normal_pckd;
layout( location = 2 ) out vec3 specular;

vec2    decode_normal(vec3 n);

void main(void)
{
    vec2 coord  = vs_pos.xy/vs_pos.w;
    vec4 tmp    = inv_proj * vec4(coord.xy,-1,1);
    vec3 dir    = tmp.xyz/tmp.w;

    // ray: { x = t*dir + x0: t > 0}
    // for the view space x0 may be (0,0,0), all "rays" goes from it, so we can optimize.
    // sphere: { x  from R^3: dot(x-vs_orig,x-vs_orig) - vs_radius^2 = 0 }
    // =>
    // 1) define t1 = min( t: dot( t*dir + x0 - vs_orig, t*dir + x0 - vs_orig) - vs_radius^2 = 0, t > 0)
    // 2) ray(t1) is the target point

    // a*t^2 + b*t + c = 0
    float a     = dot(dir,dir);
    float c     = dot(vs_orig,vs_orig) - vs_radius*vs_radius;
    vec3  tmp2  = -2*(dir*vs_orig);
    float b     = dot(tmp2,vec3(1));

    float d     = b*b - 4*a*c;

    // no intersception
    if(d<0) discard;

    // d is positive, this is a solution
    float t         = (-b - sqrt(d))*0.5/a;
    // view space position
    vec4 p          = vec4( t*dir, 1);
    p               = proj*p;
    gl_FragDepth    = 0.5*p.z/p.w + 0.5;


    // view space normal
    vec3 normal = normalize(t*dir-vs_orig);

    // transpose gives an inverse rotation of the camera
    vec3 tmp3   = (transpose(cam)*vec4(normal,0)).xyz;

    // model space coordinates
    vec3 tx     = rotate(vs_rot,tmp3);

    albedo      = vec4(texture(albedo_tx, tx.xy ).rgb*vs_color, 1);
    normal_pckd = decode_normal(normal);
    specular    = vec3(0.5);
}
