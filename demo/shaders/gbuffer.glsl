/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430 core

// set binding points to avoid massive code dublication in the host code
#ifdef RT_ALBEDO_BP
#define RT_ALBEDO_LAYOUT layout(binding=RT_ALBEDO_BP)
#else
#define RT_ALBEDO_LAYOUT
#endif

#ifdef RT_SPECULAR_BP
#define RT_SPECULAR_LAYOUT layout(binding=RT_SPECULAR_BP)
#else
#define RT_SPECULAR_LAYOUT
#endif

#ifdef RT_NORMAL_BP
#define RT_NORMAL_LAYOUT layout(binding=RT_NORMAL_BP)
#else
#define RT_NORMAL_LAYOUT
#endif

#ifdef RT_HDR_COLOR_BP
#define RT_LIGHT_LAYOUT layout(binding=RT_HDR_COLOR_BP)
#else
#define RT_LIGHT_LAYOUT
#endif

#ifdef RT_DEPTH_BP
#define RT_DEPTH_LAYOUT layout(binding=RT_DEPTH_BP)
#else
#define RT_DEPTH_LAYOUT
#endif

#ifdef TX_LUM_GMEAN_BP
#define TX_LUM_GMEAN_LAYOUT layout(binding=TX_LUM_GMEAN_BP)
#else
#define TX_LUM_GMEAN_LAYOUT
#endif
// end of layout defines

#ifndef BALL_SHININESS
#define BALL_SHININESS 18.0
#endif

#ifndef GRND_SHININESS
#define GRND_SHININESS 4.0
#endif

RT_ALBEDO_LAYOUT    uniform sampler2D rt_albedo;
RT_SPECULAR_LAYOUT  uniform sampler2D rt_specular;
RT_NORMAL_LAYOUT    uniform sampler2D rt_normal;
RT_LIGHT_LAYOUT     uniform sampler2D rt_light;
RT_DEPTH_LAYOUT     uniform sampler2D rt_depth;
TX_LUM_GMEAN_LAYOUT uniform sampler2D tx_lum_gmean;

vec2    screen_size();
vec4    load_albedo_shininess(ivec2 coord);
vec3    load_specular_color(ivec2 coord);
vec3    load_intensity(ivec2 coord);
vec3    load_position(ivec2 coord);
vec3    load_normal(ivec2 coord);
float   load_view_depth(ivec2 coord);
vec2    decode_normal(vec3 n);
vec3    load_normal(ivec2 coord);
float   load_lum_gmean();


float load_lum_gmean(){
    return texelFetch(tx_lum_gmean,ivec2(0,0),0).r;
}

vec2 screen_size(){
    return vec2(textureSize(rt_albedo,0));
}

vec4 load_albedo_shininess(ivec2 coord){
    vec4 tmp = texelFetch(rt_albedo,coord,0);
    return vec4(tmp.rgb, (tmp.a!=0 ? BALL_SHININESS : GRND_SHININESS));
}

vec3 load_specular_color(ivec2 coord){
    return texelFetch(rt_specular, coord, 0).rgb;
}

vec3 load_intensity(ivec2 coord){
    return texelFetch(rt_light,coord,0).rgb;
}

vec3 load_position(ivec2 coord){
    float depth=texelFetch(rt_depth,coord,0).x*2-1;
    vec3 pd;
    const float e=-1;
    vec2 nc=(vec2(coord)/screen_size()-vec2(0.5))*2;
    pd.z=proj[3][2]/(e*depth-proj[2][2]);
    pd.x=e*nc.x*pd.z/proj[0][0];
    pd.y=e*nc.y*pd.z/proj[1][1];
    return pd;
}

float load_view_depth(ivec2 coord){
    float depth=texelFetch(rt_depth,coord,0).x*2-1;
    const float e=-1;
    return -proj[3][2]/(e*depth-proj[2][2]);
}

// this method from paper, need to find origin
vec3 load_normal(ivec2 coord){
    vec3 n;
    n.xy=texelFetch(rt_normal,coord,0).xy;
    n.xy=n.xy*4-vec2(2);
    n.z=dot(n.xy,n.xy);
    float g=sqrt(1-n.z*0.25);
    n.xy*=g;
    n.z=1-n.z*0.5;
    return n;
}

vec2 decode_normal(vec3 n){
    float p=sqrt(n.z*8+8);
    return n.xy/p+0.5;
}
