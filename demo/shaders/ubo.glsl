/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430

#ifdef SCENE_CFG_BP
#define SCENE_CFG_LAYOUT layout (std140, binding=SCENE_CFG_BP)
#else
#define SCENE_CFG_LAYOUT layout (std140)
#endif

SCENE_CFG_LAYOUT uniform Scene_cfg
{
    float adaptation_rate;
    float adaptation_min;
    float adaptation_max;
    float dt;

    float burn_out_ratio;

    float tm_key_a;
    float tm_key_b;
    float tm_key_c;

    float pl_cut_off;
};

#ifdef DLS_SSBO_BP
#define DLS_SSBO_LAYOUT layout (std140, binding=DLS_SSBO_BP)
#else
#define DLS_SSBO_LAYOUT layout (std140)
#endif

struct DLS{
    vec3    dir;
    float   asize;
    vec3    intensity;
};

DLS_SSBO_LAYOUT buffer DLSs {
    vec3    ambient;
    float   padding;

    vec3    sky_color;
    float   padding2;

    DLS     dls[];
};

#ifdef CAM_UBO_BP
#define CAM_UBO_LAYOUT layout (std140, binding=CAM_UBO_BP)
#else
#define CAM_UBO_LAYOUT layout (std140)
#endif

CAM_UBO_LAYOUT uniform Camera
{
    mat4x4 cam;
    mat4x4 proj;
    mat4x4 inv_proj;
};

#ifdef PLS_SSBO_BP
#define PLS_SSBO_LAYOUT layout (std140, binding=PLS_SSBO_BP)
#else
#define PLS_SSBO_LAYOUT layout (std140)
#endif

struct PLS{
    vec3    position;
    float   radius;
    vec3    illumination;
    float   padding;
    vec3    attenuation_p;
};

PLS_SSBO_LAYOUT buffer PLSs{
    PLS     pls[];
};
