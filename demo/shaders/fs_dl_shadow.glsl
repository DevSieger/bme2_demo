/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430 core

flat in vec3    vs_orig;
flat in float   vs_radius;
flat in vec3    vs_ls_dir;
flat in float   vs_asize;
flat in vec3    vs_intensity;

out vec4 shadow_factor;
uniform uint ls_ix = 0;

vec3    load_position(ivec2 coord);

void main(void)
{
    ivec2   txc         = ivec2(gl_FragCoord.xy);
    vec3    fpos        = load_position(txc);
    vec3    to_b        = vs_orig-fpos;
    float   dist_to_b   = max(length(to_b),EPSILON);
    float   a_ball12    = asin(clamp(vs_radius/dist_to_b,-1,1));
    float   cos_b_ls    = -dot(to_b,vs_ls_dir)/dist_to_b;
    float   a_b_ls      = acos(clamp(cos_b_ls,-1,1));

    if( a_b_ls > a_ball12 + vs_asize*0.5) discard;
    shadow_factor   = vec4(1);
    shadow_factor[ ls_ix % 4 ]  = 1 - clamp(get_shadow_factor(a_ball12,vs_asize*0.5,a_b_ls),0,1);
}
