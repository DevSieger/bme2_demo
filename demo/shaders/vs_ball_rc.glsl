/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430

layout(location=0) in vec3  rpos;
layout(location=1) in vec3  pos;
layout(location=2) in float radius;
layout(location=3) in vec3  color;
layout(location=4) in float radiance;
layout(location=5) in vec4  rot;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

flat out vec4   vs_rot;
flat out float  vs_radius;
flat out vec3   vs_orig;
flat out vec3   vs_color;

out vec4        vs_pos;

#ifndef RADIUS_TUNE_FACTOR
#define RADIUS_TUNE_FACTOR 1.5
#endif

vec3 rotate(in vec4 quat, in vec3 v){
    return v+ 2 * cross( quat.xyz, (cross(quat.xyz,v)+quat.w*v));
}

void main(void)
{
    // we need larger ball
    float g_rad = radius*RADIUS_TUNE_FACTOR;

    // this one makes things easer, no need for gl_FragCoord minipulations
    vs_pos      = proj*cam*vec4(rotate(rot,rpos)*g_rad + pos,1 );

    // inverted rotation to get model coordinates
    vs_rot.xyz  = -rot.xyz;
    vs_rot.w    = rot.w;

    vs_radius   = radius;
    vs_color    = color;

    // the ball origin
    vs_orig     = (cam * vec4(pos,1)).xyz;

    gl_Position = vs_pos;
}
