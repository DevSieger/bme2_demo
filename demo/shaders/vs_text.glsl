/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430

layout(location=0)  in vec2 mesh_coord;
layout(location=1)  in vec4 text_color;
layout(location=2)  in vec2 origin; // in pixels
layout(location=3)  in uint character;

uniform uint        start_index;
uniform uvec2       grid_size;
uniform vec2        inv_wnd_size;
uniform sampler2D   font_sprite;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

out vec2    vs_tx_coord;
out vec4    vs_color;

void main(void)
{
    const vec2 gliph_sz_fnt = 1.0 / vec2(grid_size);
    const vec2 coord        = (vec2(textureSize(font_sprite,0))*gliph_sz_fnt*mesh_coord + origin) * inv_wnd_size * 2 - 1;
    const uint chr_ix       = character - start_index;
    const vec2 character_coord   = vec2( chr_ix % grid_size.x , grid_size.y - chr_ix / grid_size.x - 1);

    vs_tx_coord = (character_coord + mesh_coord) * gliph_sz_fnt;
    vs_color    = text_color;
    gl_Position = vec4(coord,-1,1);
}
