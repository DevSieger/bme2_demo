/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430
layout (local_size_x = 64, local_size_y = 16) in;

layout(r32f) writeonly uniform image2D img_out;

shared float storage[1024];

#ifndef ILLUMINATION_INPUT
#define ILLUMINATION_INPUT 0
#endif

#ifndef LUMINANCE_G_MEAN_OUTPUT
#define LUMINANCE_G_MEAN_OUTPUT 0
#endif

#ifndef LUMINANCE_EPSILON
#define LUMINANCE_EPSILON 0.000000000001
#endif

#ifndef IDENTITY_ELEMENT
#define IDENTITY_ELEMENT 0
#endif

#if ILLUMINATION_INPUT

vec3 load_intensity(ivec2 coord);
vec2 screen_size();

float load(ivec2 coord){
    vec2 sc_size        = screen_size();
    bool on_screen      = coord.x < sc_size.x && coord.y < sc_size.y;
    float luminance     = IDENTITY_ELEMENT;
    if(on_screen){
        const vec3 luma_w   = vec3 (0.2126,0.7152,0.0722);
        vec3 illumination   = load_intensity(coord);
        luminance           = dot(illumination, luma_w);
        luminance           = log( max(luminance, LUMINANCE_EPSILON));

        // handles the case when something is broken in the illumination calculation.
        // if we refuse to handle NaN here we can get the screen.
        if (isnan(luminance) ) luminance = IDENTITY_ELEMENT;
    }
    return luminance;
}

#else

uniform sampler2D img_in;
float load(ivec2 coord){
    return texelFetch(img_in, coord,0).x;
}

#endif

#if LUMINANCE_G_MEAN_OUTPUT

// cfg_ubo.glsl is included
// from gbuffer.glsl
float load_lum_gmean();
vec2 screen_size();

#endif

#define INDEX gl_LocalInvocationIndex

void main() {
    ivec2 tx_coord=ivec2(gl_GlobalInvocationID.x, gl_GlobalInvocationID.y*4);

    float tmp   =   load(tx_coord);
    tx_coord.y ++;
    tmp        +=   load(tx_coord);
    tx_coord.y ++;
    tmp        +=   load(tx_coord);
    tx_coord.y ++;
    tmp        +=   load(tx_coord);

    storage[INDEX]=tmp;

    memoryBarrierShared();
    barrier();
    if (INDEX<512) storage[INDEX]+=storage[INDEX+512];
    memoryBarrierShared();
    barrier();
    if (INDEX<256) storage[INDEX]+=storage[INDEX+256];
    memoryBarrierShared();
    barrier();
    if (INDEX<128) storage[INDEX]+=storage[INDEX+128];
    memoryBarrierShared();
    barrier();
    if (INDEX<64)  storage[INDEX]+=storage[INDEX+64];
    memoryBarrierShared();
    barrier();

    if (INDEX<32)  {
        storage[INDEX]+=storage[INDEX+32];
        memoryBarrierShared();
        storage[INDEX]+=storage[INDEX+16];
        memoryBarrierShared();
        storage[INDEX]+=storage[INDEX+8];
        memoryBarrierShared();
        storage[INDEX]+=storage[INDEX+4];
        memoryBarrierShared();
        storage[INDEX]+=storage[INDEX+2];
        memoryBarrierShared();
        storage[INDEX]+=storage[INDEX+1];
    }

#if LUMINANCE_G_MEAN_OUTPUT

    if (INDEX==0) {
        vec2 sc_size=screen_size();
        float old_value = load_lum_gmean();
        float new_value = exp(storage[0]/(sc_size.x*sc_size.y));
        new_value       = clamp(new_value, adaptation_min, adaptation_max);
        float value     = new_value - (new_value - old_value)* exp(-dt * adaptation_rate);
        imageStore(img_out ,ivec2(gl_WorkGroupID.xy), vec4(value,0,0,0) );
    }
#else
    if (INDEX==0)  imageStore(img_out ,ivec2(gl_WorkGroupID.xy), vec4(storage[0],0,0,0) );
#endif
}
