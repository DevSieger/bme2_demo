/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430

layout(triangles,fractional_odd_spacing,ccw) in;

in gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_in[];

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

in vec3         tc_pos[];
in vec3         tc_normal[];
patch in float  tc_radius;
patch in vec3   tc_orig;
patch in vec3   tc_color;

out vec3 te_pos;
out vec3 te_normal;

flat out    vec3 te_color;

vec3 mix3(in vec3 v0, in vec3 v1, in vec3 v2)
{
    return v0*gl_TessCoord.x + v1*gl_TessCoord.y + v2*gl_TessCoord.z;
}

void main(void)
{
    te_pos          = normalize(mix3(tc_pos[0],tc_pos[1],tc_pos[2]));
    te_normal       = normalize(mix3(tc_normal[0],tc_normal[1],tc_normal[2]));
    gl_Position     = proj*vec4(te_normal * tc_radius + tc_orig,1);
    te_color        = tc_color;
}
