/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#version 430

layout (vertices=3) out;

in gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_in[];

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
} gl_out[];

in vec3     vs_pos[];
in vec3     vs_normal[];
in float    vs_radius[];
in vec3     vs_orig[];
in vec3     vs_color[];

out vec3    tc_pos[];
out vec3    tc_normal[];

patch out float tc_radius;
patch out vec3  tc_orig;
patch out vec3  tc_color;

uniform vec2 threshold_factor;

vec2 get_pos(int ix){
    return gl_in[ix].gl_Position.xy / gl_in[ix].gl_Position.w * threshold_factor;
}

void main(void)
{
    vec2 tmp[3]     = { get_pos(0), get_pos(1), get_pos(2) };
    const int a[3]  = {1,0,0};
    const int b[3]  = {2,2,1};

    gl_TessLevelOuter[gl_InvocationID]  = max(distance(tmp[a[gl_InvocationID]],tmp[b[gl_InvocationID]]),3);
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
    tc_pos[gl_InvocationID]             = vs_pos[gl_InvocationID];
    tc_normal[gl_InvocationID]          = vs_normal[gl_InvocationID];

    barrier();

    if( gl_InvocationID == 0 ){
        tc_radius   = vs_radius[0];
        tc_orig     = vs_orig[0];
        tc_color    = vs_color[0];
        gl_TessLevelInner[0] = max(max(gl_TessLevelOuter[0],gl_TessLevelOuter[1]),gl_TessLevelOuter[2]);
    }
}
