cmake_minimum_required(VERSION 2.8)

include(CheckIncludeFileCXX)

set(FS_FIX_TEST_FILE ${CMAKE_CURRENT_LIST_DIR}/fs_fix/fs_user.cpp)

# workaround for std::file_system linking
function ( get_std_file_system_lib result_var )
    # test if <filesystem> may be at least included
    # is -DCMAKE_CXX_STANDARD=17 required?
    set(CMAKE_REQUIRED_FLAGS "-DCMAKE_CXX_STANDARD=17")
    CHECK_INCLUDE_FILE_CXX("filesystem" HAS_STD_FS_SUPPORT)
    if(NOT "${HAS_STD_FS_SUPPORT}")
        message(FATAL_ERROR "compiler doesn't support std::file_system")
    endif()

    if(NOT DEFINED "${result_var}")

        # is it better to test in the reverse order?
        # i.e. to use additional library anyway if it is possible.

        function ( try_compile_fs_test result_var lib )
            try_compile(
                ${result_var} ${CMAKE_BINARY_DIR} ${FS_FIX_TEST_FILE}
                CMAKE_FLAGS -DCMAKE_CXX_STANDARD=17
                LINK_LIBRARIES ${lib}
                OUTPUT_VARIABLE COMPILATION_LOG)
            set(${result_var} ${${result_var}} PARENT_SCOPE)
        endfunction(try_compile_fs_test)

        message(STATUS "Test if a code that uses std::file_system requires a special linking")
        try_compile_fs_test(FS_USER_COMPILED "")

        if(FS_USER_COMPILED)
            message(STATUS "std::file_system doesn't require a special linking")
            set(${result_var} "" CACHE INTERNAL "TEST ${result_var}")
            return()
        else()
            message(STATUS "std::file_system requires a special linking")
        endif()

        message(STATUS "Trying to build a code that uses std::file_system, linking with stdc++fs library")
        try_compile_fs_test(FS_USER_COMPILED stdc++fs)

        if(FS_USER_COMPILED)
            message(STATUS "std::file_system requires stdc++fs library")
            set(${result_var} stdc++fs CACHE INTERNAL "TEST ${result_var}")
            return()
        endif()

        message(STATUS "Trying to build a code that uses std::file_system, linking with c++fs library")
        try_compile_fs_test(FS_USER_COMPILED c++fs)

        if(FS_USER_COMPILED)
            message(STATUS "std::file_system requires c++fs library")
            set(${result_var} c++fs CACHE INTERNAL "TEST ${result_var}")
            return()
        endif()

        message(FATAL_ERROR "std::file_system can't be used")
    endif()
endfunction( get_std_file_system_lib )
