#include <filesystem>
#include <iostream>

namespace fs = std::filesystem;

int main(int argc, char** argv){
    fs::path path(argv[0]);
    path    = fs::canonical(path);
    if( !fs::is_regular_file(path) ) return 1;
    path   /= "test";
    std::cout << path << std::endl;
}
