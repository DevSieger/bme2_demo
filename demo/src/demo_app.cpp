/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "demo_app.hpp"
#include "renderer.hpp"
#include "camera.hpp"
#include "scene_data.hpp"
#include "scene.hpp"
#include "state_manager.hpp"
#include <dk/timer.hpp>
#include "camera_control.hpp"
#include "gui_renderer.hpp"
#include "gui.hpp"
#include "bme2_scene.hpp"
#include <algorithm>
#include <map>

namespace fgs_demo{

constexpr char state_file_path [] = "./state.txt";

struct Demo_app::Data {
    Data( const Resource_loader& loader):
        gui_renderer(loader)
    {}

    Renderer        renderer;
    GUI_renderer    gui_renderer;
    Help_screen     hlp_scrn;
    Performace_gui  pgui;
    Settings_gui    sgui;
    Settings_gui    sc_settings_gui;
    GUI*        current_gui = nullptr;
    dk::Timer   timer;
    double      time = 0;
    glm::dvec2  last_cursor_pos;
    State_manager state_mngr;
    std::unique_ptr<Camera_control> cam_ctrl;
    std::unique_ptr<Scene>          scene;
    Camera_type cam_type    {Camera_type::LOOK_AT };
    Scene_ix    scene_type  {Scene_ix::BME2};
    struct{
        // can be loaded from a file
        struct{
            float   min     = 2;
            float   max     = 64;
            float   init    = 8;
            float   factor  = 2;
        } cam_speed;
    } cfg;
    std::map<std::string, Font_ptr, std::less<>> font_cache;
    Camera_control::Key_status_fn key_reporter;
};

Demo_app::Demo_app(const Resource_loader& loader):
    loader_(loader)
{
}

Demo_app::~Demo_app()
{
}

void Demo_app::do_draw() {
    double dt = data().timer.restart();

    GUI* cur_gui    = data().current_gui;
    using Kb_fn     = Camera_control::Key_status_fn;
    data().cam_ctrl->update(dt,(!cur_gui || cur_gui->camera_control_enabled()) ? data().key_reporter : Kb_fn{});
    data().pgui.update_performance(dt);


    if(cur_gui){
        cur_gui->update(dt);
        data().gui_renderer.upload(cur_gui->gui_data(),dt);
    }

    if(data().scene){
        data().scene->update(dt);
        data().renderer.draw(data().scene->scene_data(),dt);
    }

    if(cur_gui) data().gui_renderer.draw();
}

Settings Demo_app::make_settings(){
    Settings settings; // NRVO
    auto& renderer = data().renderer;
    using Sz    = std::size_t;
    using NL    = std::initializer_list<Setting::Name>;

    auto add_setting = [&settings](Setting::Name&& s, auto&& action, NL && options, Sz active = 0 ) -> void {
        settings.emplace_back(std::move(s),std::forward<decltype(action)>(action),std::move(options),active);
    };

    auto ball_shading_f = [&renderer](Sz ix) -> void{
        switch (ix) {
            case 0:{
                renderer.ball_draw_mode(Renderer::Ball_draw_mode::RECONSTRUCTION);
            }; return ;
            case 1:{
                renderer.ball_draw_mode(Renderer::Ball_draw_mode::TESSELATION);
            }; return ;
        }
    };
    Sz def_bs   = renderer.ball_draw_mode() == Renderer::Ball_draw_mode::TESSELATION;
    Sz def_wm   = renderer.wire_mode();
    Sz def_perf = renderer.performance_counting();
    add_setting("ball shading", std::move(ball_shading_f),{"reconstruction", "tesselation"},def_bs);
    add_setting("ball wire mode", [&renderer](std::size_t ix) -> void{ renderer.wire_mode(ix);}, {"disabled","enabled"},def_wm);
    add_setting("section statistic", [&renderer](std::size_t ix){ renderer.performance_counting(ix);}, {"disabled","enabled"},def_perf);
    add_setting("window mode", [this](std::size_t ix){ full_screen(ix);}, {"windowed","full screen"}, full_screen());
    add_setting("vsync", [this](std::size_t ix){ vsync(ix);}, {"disabled","enabled"}, vsync());
    return settings;
}

std::string_view Demo_app::control_string() const
{
    return
R"cntrlstr(
W   - move forward  (previous setting)
S   - move backward (next setting)
A   - move left     (previous option)
D   - move right    (next option)

R   - move camera up
F   - move camera down
E   - change camera speed

F1  - show controls     F3  - show preformance
F4  - save state        F8  - load state
F11 - scene settings    F12 - rendering setting
ESC - hide UI

Space   - "relaunch" balls
)cntrlstr";
}

void Demo_app::camera_type(Camera_type type)
{
    assert(data().scene);
    auto& cctrl = data().cam_ctrl;
    auto& cam   = data().scene->cam();

    // not so elegant, but
    if(cctrl && &cctrl->camera() == &cam){
        // already done
        if(data().cam_type == type ) return;

        // we need to copy the common state from the previous control
        switch (type) {
            case Camera_type::LOOK_AT : {
                cctrl   = std::make_unique<Point_watch>(*cctrl);
            };break;
            case Camera_type::FREE : {
                cctrl   = std::make_unique<First_person_view>(*cctrl);
            };break;
        }
    } else {
        switch (type) {
            case Camera_type::LOOK_AT : {
                cctrl   = std::make_unique<Point_watch>(cam);
            };break;
            case Camera_type::FREE : {
                cctrl   = std::make_unique<First_person_view>(cam);
            };break;
        }
        cctrl->velocity(data().cfg.cam_speed.init);
    }

    cursor_mode(!cctrl->cursor_enabled());
    data().cam_type         = type;
}

void Demo_app::switch_scene(Demo_app::Scene_ix sc)
{
    auto& sc_ptr    = data().scene;

    // reset control of the camera
    data().cam_ctrl = nullptr;

    switch (sc) {
        case Scene_ix::Test_scene: {
            sc_ptr = std::make_unique<Test_scene>();
        }; break;
        case Scene_ix::Benchmark: {
            sc_ptr = std::make_unique<Benchmark_scene>();
        }; break;
        case Scene_ix::Snapshot: {
            Scene_data tmp;
            data().state_mngr.restore_from_file(tmp,state_file_path);
            sc_ptr = std::make_unique<Snapshot_scene>(tmp);
        }; break;
        case Scene_ix::BME2: {
            sc_ptr = std::make_unique<Bme2_scene>();
        }
    }
    data().scene_type   = sc;
    camera_type(data().cam_type);
    rebuild_scene_settings();
}

void Demo_app::switch_gui(GUI* gui)
{
    data().current_gui = gui;
}

void Demo_app::rebuild_scene_settings()
{
    auto& sc_ptr = data().scene;
    assert(sc_ptr);
    Settings tmp = sc_ptr->make_settings();
    using NL = std::initializer_list<Setting::Name>;
    Settings st;
    using Sz = Setting::Size;
    st.emplace_back("switch scene",[this](Sz ix){ switch_scene(Scene_ix(ix));},NL{"test scene","benchmark","last snapshot","bme2"}, Sz(data().scene_type));
    st.emplace_back("camera mode", [this](Sz ix){ camera_type(Camera_type(ix));},NL{"look at center","free"}, Sz(data().cam_type));
    std::move(tmp.begin(),tmp.end(),std::back_inserter(st));
    auto font_loader = [this](std::string_view fnt){ return load_font(fnt);};
    data().sc_settings_gui.build(font_loader, "Scene settings:", std::move(st));
}

Font_ptr Demo_app::load_font(std::string_view path)
{
    auto& cache = data().font_cache;
    auto it = cache.find(path);
    Font_ptr res;
    if( it == cache.end() ){
        // the single layout for all current ASCII fonts.
        res = data().gui_renderer.load(Font_info::Path(path),{94,1},'!');
        cache.emplace(path,res);
    } else {
        res = it->second;
    }
    return res;
}

void Demo_app::cursor_mode(bool locked)
{
    if(cursor_lock()!=locked){
        cursor_lock(locked);
        auto pos                = cursor_position();
        data().last_cursor_pos  = {pos.x,pos.y};
    }
};

void Demo_app::do_init(Size width, Size height){
    m_ = std::make_unique<Data>(loader_);
    data().key_reporter = [this](int key) -> bool { return key_state(key) == Action::PRESS; };
    data().renderer.init(loader_, width, height);
    data().gui_renderer.init(width, height);

    switch_scene(data().scene_type);
    camera_type(data().cam_type);

    auto font_loader = [this](std::string_view fnt){ return load_font(fnt);};
    data().pgui.build(font_loader);
    data().pgui.statistic_cb([this]()-> decltype(auto) {return data().renderer.get_stat();});

    data().sgui.build(font_loader, "Rendering settings:", make_settings() );

    data().hlp_scrn.build(font_loader,"Controls:", control_string());
    vsync(1);

    data().timer.start();

    switch_gui(nullptr);
}

void Demo_app::on_mouse_move(double x, double y){
    using glm::dvec2;
    dvec2 cur{x,y};
    auto& last      = data().last_cursor_pos;
    dvec2 dx        = cur - last;
    last            = cur;

    auto& cur_gui   = data().current_gui;
    if( !cur_gui || cur_gui->camera_control_enabled()){
        data().cam_ctrl->on_mouse_move_delta(dx.x,dx.y);
    }
}

void Demo_app::on_resize(Size width, Size height)
{
    if(!width || !height) return;
    data().renderer.resize(width,height);
    data().gui_renderer.resize(width,height);
}

void Demo_app::on_key(int key, int mods, Action act) {
    // at first let gui try to handle it.
    if(data().current_gui && data().current_gui->on_key(key,mods,act)) return;

    if(data().scene && data().scene->on_key(key,mods,act)) return;

    assert(data().cam_ctrl);
    auto& cctrl = *data().cam_ctrl;

    if(act==Action::PRESS) switch (key) {
        case 'E':{
            auto& cam_spd   = data().cfg.cam_speed;
            float vel = cctrl.velocity();
            vel *= cam_spd.factor;
            if(vel>cam_spd.max) vel = cam_spd.min;
            cctrl.velocity(vel);
        }; return;

        // ESC
        case 256:{
            switch_gui(nullptr);
        }; return;

        // F1
        case 290:{
            switch_gui(&data().hlp_scrn);
        }; return;

        // F3
        case 292:{
            switch_gui(&data().pgui);
        }; return;

        // F4
        case 293:{
            if(data().scene){
                data().state_mngr.save_to_file(data().scene->scene_data(),state_file_path);
            }
        }; return;
        // F8
        case 297:{
            switch_scene(Scene_ix::Snapshot);
        }; return;
        // F11
        case 300:{
            switch_gui(&data().sc_settings_gui);
        }; return;
        // F12
        case 301:{
            switch_gui(&data().sgui);
        }; return;

    }
}

} // namespace fgs_demo
