/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "demo_app.hpp"
#include <iostream>

#define DK_LOGGING_IMPLEMENTATION
#include <dk/logging.hpp>

#include "fs_resource_loader.hpp"

void init_logging(){
    dk::init_logging("./log.txt");
    auto& logger = dk::get_logger();
    using Str   = dk::Logger::String;
    using D_lvl = dk::Detail_level;
    dk::Logger::Output new_outf = [old_out = logger.output()](D_lvl lvl, Str&& msg, const std::locale& loc) -> void{
        std::ostream& ostr = (lvl <= D_lvl::DEBUG)? std::cout : std::cerr;
        if(ostr.getloc() != loc){
            ostr.imbue(loc);
        }
        ostr<<msg;
        old_out(lvl,std::move(msg),loc);
    };
    logger.output(new_outf);
}

int main(int argc, char** argv)
{
    init_logging();
    fgs_demo::Fs_resource_loader loader(argv[0]);
    fgs_demo::Demo_app app(loader);
    app.title("demo").size(1024,720);
    app.run();
    return 0;
}
