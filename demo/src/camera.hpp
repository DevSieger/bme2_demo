/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

namespace fgs_demo {

class View_position{
public:
    using Rotation  = glm::quat;
    using Position  = glm::vec3;
    using Direction = glm::vec3;

    /// Returns a forward direction in the camera space coordinates
    ///
    /// this is not {0,0,-1}, "camera space" isn't "view space".
    /// the view does'n look down by default
    static constexpr Direction cs_forward(){
        return {0, 1.0f, 0};
    }

    /// Returns a up direction in the camera space coordinates
    static constexpr Direction cs_up(){
        return {0, 0, 1.0f};
    }

    /// Returns a forward direction of this view.
    Direction direction() const{
        return rot_ * cs_forward();
    }

    /// Returns the up direction of this view.
    Direction up() const{
        return rot_ * cs_up();
    }

    /*!
     * \brief Sets a direction of the view .
     * \param new_dir The specified new view direction.
     * \param new_up A new up direction that determines a new orientation of this view.
     * \return This object.
     */
    View_position& direction(Direction new_dir, Direction new_up = {0,0,1});

    /*!
     * \brief Returns the rotation.
     * \return The quaternion defining a transform from the initial view direction to the current view direction.
     */
    const Rotation& rot() const{
        return rot_;
    }

    /*!
     * \brief Sets the rotation.
     * \param p rot The quaternion defining a transform from the initial view direction to the new view direction.
     * \return This object.
     */
    View_position& rot(const Rotation& p_rot){
        rot_ = p_rot;
        return *this;
    }

    /// Returns the position of an origin of the view.
    const Position& pos()const{
        return pos_;
    }

    /*!
     * \brief Sets the position of an origin of the view.
     * \return This object.
     */
    View_position& pos(const Position& p_pos){
        pos_ = p_pos;
        return *this;
    }

private:
    Rotation rot_{1,0,0,0};
    Position pos_{0,0,0};
};

class Camera
{
public:
    using Transform = glm::mat4x4;

    /// returns the position of the camera
    const View_position& view_position() const{
        return pos_;
    }

    /// gives access to the position of the camera
    View_position& view_position(){
        return pos_;
    }

    /// Returns the field of view of this camera in radians.
    float fov()const{
        return fov_;
    }

    /*!
     * \brief Sets the field of view of this camera.
     * \param p_fov The field of view in radians.
     * \return This camera.
     */
    Camera& fov(float p_fov){
        fov_=p_fov;
        return *this;
    }

    /// Returns the distance from the position of this camera to the nearest plane of this camera's view frustum.
    float nearest()const{
        return near_;
    }

    /// Sets the distance from the position of this camera to the nearest plane of this camera's view frustum.
    Camera& nearest(float p_near){
        near_=p_near;
        return *this;
    }

    /// Returns the distance from the position of this camera to the farthest plane of this camera's view frustum.
    float farthest()const{
        return far_;
    }

    /// Sets the distance from the position of this camera to the farthest plane of this camera's view frustum.
    Camera& farthest(float p_far){
        far_=p_far;
        return *this;
    }

    /// Returns the view matrix
    Transform view_matrix() const; // world space -> view space

    /*!
     * \brief Returns the projection matrix
     * \param width     the width of the view port
     * \param height    the height of the view port
     * \return the projection matrix
     */
    Transform projection_matrix(float width, float height) const; // view space -> screen space

private:
    View_position pos_;
    float near_ = 0.5;
    float far_  = 500;
    float fov_  = glm::pi<float>()*0.25f;
};

} // namespace fgs_demo
