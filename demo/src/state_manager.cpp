/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "state_manager.hpp"
#include <fstream>
#include <dk/logging.hpp>
#include <string>
#include <cstdio>
#include <type_traits>

namespace fgs_demo {

namespace{

template<typename T, typename U>
using sfinae_t = std::enable_if_t<std::is_same_v<std::decay_t<T>, U>>*;

// Cam is template class parameter to handle const-qualifiers without code dublication
template<typename F, typename Cam>
decltype (auto) bind_to_io(F f, Cam&& cam, sfinae_t<Cam, Camera> = nullptr){
    constexpr char fmt[] =
R"(
camera:{
    position:{%f, %f, %f},
    rotation:{%f, %f, %f, %f}
};)";
    auto& vp = cam.view_position();
    return f(fmt, vp.pos().x,vp.pos().y,vp.pos().z,
      vp.rot().w,vp.rot().x,vp.rot().y,vp.rot().z);
}

struct Scene_data_info{
    using Size = std::uint32_t;
    Size    n_balls;
    Size    n_dl;
    Size    n_pl;
    Vec3    sky_color;
    Vec3    ambient_color;
};

template<typename F, typename T>
decltype (auto) bind_to_io(F f, T&& info, sfinae_t<T, Scene_data_info> = nullptr){
    constexpr char fmt[] =
R"(scene_info: {
    sky_color:{%f, %f, %f},
    ambient_color:{%f, %f, %f},
    num_balls:%u,
    num_directional_lights:%u,
    num_point_lights:%u
};)";
    auto& sc    = info.sky_color;
    auto& ac    = info.ambient_color;
    return f(fmt, sc.x, sc.y, sc.z, ac.x, ac.y, ac.z, info.n_balls, info.n_dl, info.n_pl);
}

template<typename F, typename T>
decltype (auto) bind_to_io(F f, T&& ball, sfinae_t<T, Ball> = nullptr){
    constexpr char fmt[] =
R"(
ball:{
    pos:{%f, %f, %f},
    radius:%f,
    color:{%f, %f, %f},
    radiance:%f,
    rotation:{%f,%f,%f,%f}
};)";
    return f(fmt,
             ball.pos.x, ball.pos.y, ball.pos.z,
             ball.radius,
             ball.color.x, ball.color.y, ball.color.z,
             ball.radiance,
             ball.rotation.w, ball.rotation.x, ball.rotation.y, ball.rotation.z
             );
}

template<typename F, typename T>
decltype (auto) bind_to_io(F f, T&& pl, sfinae_t<T, Point_light> = nullptr){
    constexpr char fmt[] =
R"(
point_light:{
    pos:{%f, %f, %f},
    radius:%f,
    illumination:{%f, %f, %f},
    attenuation_p:{%f,%f,%f}
};)";
    return f(fmt,
             pl.pos.x, pl.pos.y, pl.pos.z,
             pl.radius,
             pl.illumination.x, pl.illumination.y, pl.illumination.z,
             pl.attenuation_p.x, pl.attenuation_p.y, pl.attenuation_p.z
             );
}

template<typename F, typename T>
decltype (auto) bind_to_io(F f, T&& dl, sfinae_t<T, Directional_light> = nullptr){
    constexpr char fmt[] =
R"(
directional_light:{
    direction:{%f, %f, %f},
    radius:%f,
    illumination:{%f, %f, %f}
};)";
    return f(fmt,
             dl.direction.x, dl.direction.y, dl.direction.z,
             dl.radius,
             dl.illumination.x, dl.illumination.y, dl.illumination.z
             );
}

// returns an output iterator to element next to the last element written
template<typename T, typename Out_it>
Out_it serialize(const T& obj, Out_it out){
    auto get_size = [](const char* fmt, auto&&... args) -> int {
        return std::snprintf(nullptr,0,fmt,args...);
    };
    int len = bind_to_io(get_size, obj);
    assert(len >= 0);

    std::vector<char> res(len+1);

    auto print = [&res] (const char* fmt, auto&&... args) -> int {
        return std::snprintf(res.data(),res.size(),fmt,args...);
    };
    bind_to_io(print, obj);
    res.pop_back();
    return std::copy(res.cbegin(),res.cend(),out);
}

// returns a pointer to the end of the deserialized range
template <typename Obj>
const char* deserialize(const char* src, Obj& obj){
    int sz = 0;
    auto f = [src, &sz](const char* fmt, auto&&... args) -> bool{
        std::string tmp;
        tmp.reserve(std::strlen(fmt)+2);
        tmp += fmt;
        tmp += "%n";
        int res = sscanf(src, tmp.c_str() ,&args..., &sz);
        return res == sizeof... (args);
    };
    [[maybe_unused]] bool res = bind_to_io(f,obj);
    assert(res);
    return src + sz;
}

}

void State_manager::save_to_file(const Scene_data& sc, const char* path)
{
    std::ofstream file(path,std::ios_base::binary);
    if(file.is_open()){
        Scene_data_info sdi;
        sdi.ambient_color   = sc.ambient_light;
        sdi.sky_color       = sc.sky_color;
        sdi.n_balls         = sc.balls.size();
        sdi.n_pl            = sc.point_lights.size();
        sdi.n_dl            = sc.directional_lights.size();
        auto it = std::ostreambuf_iterator<char>(file);
        it = serialize(sdi,it);
        it = serialize(sc.cam,it);
        for(auto& b: sc.balls){
            it = serialize(b,it);
        }
        for(auto& dl: sc.directional_lights){
            it = serialize(dl,it);
        }
        for(auto& pl: sc.point_lights){
            it = serialize(pl,it);
        }
    }else{
        dk::log(dk::Detail_level::CRITICAL,"can't open file %1% to save the state", path);
    }
}

void State_manager::restore_from_file(Scene_data& sc, const char* path)
{
    std::string content;
    std::ifstream file(path, std::ios_base::binary);
    if(!file.is_open()){
        dk::log(dk::Detail_level::CRITICAL,"can't open file %1%",path);
        return;
    }
    file.seekg(0,std::ios_base::end);
    auto file_size=file.tellg();
    if(file_size>0){
        file.seekg(0,std::ios_base::beg);
        content.resize(file_size);
        file.read(&content[0],file_size);
    }
    Scene_data_info sdi;
    const char* cur = content.data();
    cur = deserialize(cur,sdi);
    sc.ambient_light    = sdi.ambient_color;
    sc.sky_color        = sdi.sky_color;
    cur = deserialize(cur,sc.cam);

    auto deserialize_cont = [&cur](auto& v, std::size_t sz){
        v.resize(sz);
        for(std::size_t i =0; i < sz; ++i){
            cur = deserialize(cur, v[i]);
        };
    };

    deserialize_cont(sc.balls, sdi.n_balls);
    deserialize_cont(sc.directional_lights, sdi.n_dl);
    deserialize_cont(sc.point_lights, sdi.n_pl);
}

} // namespace fgs_demo
