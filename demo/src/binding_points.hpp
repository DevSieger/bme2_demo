/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

namespace fgs_demo {

namespace tx_binding_points {
// MAX_COMBINED_TEXTURE_IMAGE_UNITS is at least 96
enum: unsigned int{
    // 0 is reserved
    RT_ALBEDO       = 1,
    RT_SPEC,
    RT_DEPTH,
    RT_HDR_COLOR,
    RT_NORMAL,
    TX_GRND_ALBEDO,
    TX_GRND_NORMAL,
    TX_GRND_SPEC,
    TX_BALL_ALBEDO,
    TX_LUM_GMEAN,
    TX_SHADOW_TMP_DL,
    CS_INPUT_A,
    GUI_FONT_SPRITE,
    TOTAL
};
} // namespace tx_binding_points

namespace img_binding_points {
// MAX_IMAGE_UNITS is at least 8
enum: unsigned int{
    CS_OUTPUT_A
};
} // namespace img_binding_points

namespace ubo_binding_points {
enum: unsigned int{
    CAMERA,
    CFG
};
} // namespace ubo_binding_points

namespace ssbo_binding_points {
enum: unsigned int{
    DIRECTIONAL_LS,
    POINT_LS
};
} // namespace ssbo_binding_points

} // namespace fgs_demo
