/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/fgs.hpp>
#include "shared_storage.hpp"

namespace fgs_demo {

class Full_screen_drawer
{
public:
    void init();
    void draw();
private:
    fgs::VAO    vao_;
    fgs::Buffer vbo_ {GL_ARRAY_BUFFER, GL_STATIC_DRAW};
};

class Ball_drawer{
public:
    Ball_drawer(Shared_storage& st);
    void init();
    void draw(bool tesselation = false);

    /// returns a radius of the largest sphere laying inside the mesh of a ball
    static float inner_radius();
private:
    fgs::VAO    vao_;
    fgs::Buffer vbo_        {GL_ARRAY_BUFFER,           GL_STATIC_DRAW};
    fgs::Buffer ibo_        {GL_ELEMENT_ARRAY_BUFFER,   GL_STATIC_DRAW};

    unsigned        num_instances() const;
    fgs::Buffer&    instance_buffer();
    Shared_storage& storage_;
};

class Point_light_drawer{
public:
    Point_light_drawer(Shared_storage& st);
    void init();
    void draw(GLuint start, GLsizei count);

    /// returns a radius of the largest sphere laying inside the mesh
    static float inner_radius();
private:
    fgs::VAO    vao_;
    fgs::Buffer vbo_        {GL_ARRAY_BUFFER,           GL_STATIC_DRAW};
    fgs::Buffer ibo_        {GL_ELEMENT_ARRAY_BUFFER,   GL_STATIC_DRAW};

    unsigned        num_instances() const;
    fgs::Buffer&    instance_buffer();
    Shared_storage& storage_;
};

class Dl_shadow_drawer{
public:
    Dl_shadow_drawer(Shared_storage& st);
    void init();
    void draw();

    /// returns a radius of the largest cylinder laying inside the mesh
    static float inner_radius();
private:
    fgs::VAO    vao_;
    fgs::Buffer vbo_        {GL_ARRAY_BUFFER,           GL_STATIC_DRAW};
    fgs::Buffer ibo_        {GL_ELEMENT_ARRAY_BUFFER,   GL_STATIC_DRAW};

    unsigned        num_instances() const;
    fgs::Buffer&    instance_buffer();
    Shared_storage& storage_;
};

} // namespace fgs_demo

