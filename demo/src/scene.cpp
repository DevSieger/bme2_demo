/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "scene.hpp"
#include "misc.hpp"

namespace fgs_demo {

void Scene::update(double dt)
{
    do_update(dt);
}

namespace {

glm::vec3 rnd_color(){
    static std::normal_distribution<float> r(0.5, 0.5/3);
    static std::normal_distribution<float> g(0.5, 0.5/3);
    static std::normal_distribution<float> b(0.5, 0.5/3);
    auto comp = [](auto& dist) -> float { return glm::clamp<float>(dist(rnd_engige()),0.0,1.0);};
    return {comp(r),comp(g),comp(b)};
}

}

//======== Test_scene ========//

Test_scene::Test_scene()
{
    auto& sc = data();
    auto& vpos = sc.cam.view_position();
    vpos.pos(View_position::Position{2,-19,3});
    vpos.direction(-vpos.pos());

    sc.balls.emplace_back(Ball{Vec3{6,0,4},4,{0.5,0.5,1},0.2,Rotation(1,0,0,0)});
    sc.balls.emplace_back(Ball{Vec3{0,0,2},2,{0.2,0.5,1},0.2, Rotation(1,0,0,0)});

    const float dl_scale    = 1;
    const float pl_scale    = 5;
    const float am_scale    = 1;
    const float sc_scale    = 1;

    Directional_light sun {Vec3(0,1,-2),glm::pi<float>()/24.0f,Vec3{5.1,5.2,8.5}*dl_scale};
    sc.directional_lights.emplace_back(sun);
    sun.direction = Vec3(-1,0,-2);
    sun.illumination = Vec3(8.5,6.3,5.1)*dl_scale;
    sc.directional_lights.emplace_back(sun);

    sc.ambient_light    = Vec3(1.75,1.75,2)*am_scale;
    sc.sky_color        = Vec3(1.3,2.5,4)*sc_scale;

    sc.point_lights.push_back( make_point_light(Vec3{-4,-4,5},0.2,Vec3{1,3,1}*pl_scale,5) );
    sc.point_lights.push_back( make_point_light(Vec3{12,0,2},0.2,Vec3{4,2,1}*pl_scale,3) );
}

void Test_scene::do_update(double dt)
{
    time_      += dt;
    Rotation nr = glm::angleAxis( glm::pi<float>()*(float)time_*0.05f, glm::normalize(Vec3{0,0,1}));
    data().balls[1].rotation = nr;
}

//======== Benchmark_scene ========//
namespace  {

constexpr glm::uvec3    default_bm_grid(4u,4u,4u);
constexpr unsigned      default_bm_pls = 0;

}

Benchmark_scene::Benchmark_scene()
{
    auto& vpos = data().cam.view_position();
    vpos.pos(Vec3(8,-16,8));
    vpos.direction(-vpos.pos());
    build();
    build_balls(default_bm_grid);
    build_pls(default_bm_pls);
}

Settings Benchmark_scene::make_settings()
{
    Settings st;
    using Sz    = Setting::Size;
    using NL    = std::initializer_list<Setting::Name>;
    auto nbl    = [this](Sz ix){
        switch (ix) {
            case 0:{
                build_balls(default_bm_grid);
            }; break;
            case 1:{
                build_balls(Grid(8,8,8));
            }; break;
            case 2:{
                build_balls(Grid(10,10,10));
            }; break;
        }
    };
    auto npl    = [this](Sz ix){
        switch (ix) {
            case 0:{
                build_pls(default_bm_pls);
            }; break;
            case 1:{
                build_pls(4);
            }; break;
            case 2:{
                build_pls(8);
            }; break;
            case 3:{
                build_pls(16);
            }; break;
            case 4:{
                build_pls(32);
            }; break;
        }
    };
    st.emplace_back("the number of balls",std::move(nbl),NL{"64","512","1000"}, 0);
    st.emplace_back("the number of point lights",std::move(npl),NL{"0","4","8","16","32"}, 0);
    return  st;
}

void Benchmark_scene::build()
{
    auto& sc = data();

    const float dl_scale    = 0.5;
    const float am_scale    = 0.3;
    const float sc_scale    = 0.3;

    const Vec3 amb_color    = Vec3(0.2,0.4,1);

    sc.directional_lights.clear();
    Directional_light sun {Vec3(2,5,-7),glm::pi<float>()/24.0f,Vec3{1,1,1}*dl_scale};
    sc.directional_lights.emplace_back(sun);

    sc.ambient_light    = amb_color * am_scale;
    sc.sky_color        = amb_color * sc_scale;
}

void Benchmark_scene::build_balls(Benchmark_scene::Grid balls)
{
    grd_ = balls;
    auto& sc = data();
    sc.balls.clear();
    unsigned ix = 0;
    float side  = glm::min(32u,balls.x*2);
    float step  = side/balls.x;
    float br    = step*0.25f;
    for(float z = br, lz = br + step*balls.z; z < lz; z+=step){
        for(float y = 0.5f*step*(1.0f - balls.y), ly = -y; y<=ly; y+=step){
            for(float x = 0.5f*step*(1.0f - balls.x), lx = -x; x <= lx; x+=step, ++ix){
                sc.balls.emplace_back(Ball{Vec3{x,y,z},br,rnd_color(),0.2,Rotation(1,0,0,0)});
            }
        }
    }
}

void Benchmark_scene::build_pls(unsigned n_pl)
{
    const float pl_scale    = 5;
    using glm::cos, glm::sin, glm::pi;
    auto& sc = data();
    sc.point_lights.clear();
    for(unsigned i = 0; i<n_pl; ++i){
        const float r   = 9;
        const float a   = i*2*pi<float>()/n_pl;
        Vec3 pos (r*cos(a),r*sin(a),5);
        sc.point_lights.push_back( make_point_light(pos,0.2,rnd_color()*pl_scale,4) );
    }
}

void Benchmark_scene::do_update(double dt)
{
    time_  += dt;
    if(time_ > 1){
        const float max_range   = 24;
        const float z_mean      = 4;
        std::normal_distribution<float> dist_xy(0, max_range/3);
        std::normal_distribution<float> dist_z(z_mean, z_mean/3);
        const float pl_scale    = 5;

        for(auto& pl: data().point_lights){
            Vec3 pos(dist_xy(rnd_engige()),dist_xy(rnd_engige()),dist_z(rnd_engige()));
            pl = make_point_light(pos,0.2,rnd_color()*pl_scale,4);
        }
        time_ = 0;
    }
}

} // namespace fgs_demo
