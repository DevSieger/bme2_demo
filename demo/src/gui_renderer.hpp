/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include "gui_data.hpp"
#include <forward_list>
#include <fgs/fgs.hpp>
#include "font.hpp"
#include "resource_loader.hpp"

namespace fgs_demo {

/// A stand alone renderer for GUI. Draws text labels.
class GUI_renderer
{
public:
    GUI_renderer(const Resource_loader& loader);
    void init(unsigned width, unsigned height);
    void resize(unsigned width, unsigned height);

    // draws uploaded GUI over existing content of the default framebuffer
    void draw();
    void upload(const GUI_data& gui, float dt);

    // throws std::runtime_error on failure
    // a lifetime of the managed font object ends BEFORE a lifetime of this object ANYWAY.
    // UB when this object is destroyed before the returned Font_ptr.
    Font_ptr load(const Font_info::Path& path, Font_info::Size grid, Font::Char_ix first_char_index);

private:
    using Fonts = std::forward_list<Font>;

    // complexity: O(fonts_.size()).
    // fonts_.size() assumed to be small.
    void unload(Font_info* ptr);

    Fonts               fonts_;
    glm::vec2           window_size_;   //yes, floats.
    fgs::VAO            vao_;
    fgs::Buffer         vbo_        {GL_ARRAY_BUFFER, GL_STATIC_DRAW};
    fgs::Buffer         instances_  {GL_ARRAY_BUFFER, GL_STREAM_DRAW};
    fgs::Shader_program shader_     {"text_shader"};

    // reason: some characters should be skipped when drawing.
    // the size of the label text may be greater than the numer of uploaded characters
    struct Draw_info{
        std::shared_ptr<const Font> font;
        std::size_t start;  // index of the first charcter of the label in the instance data
        std::size_t count;  // the number of uploaded characters of the label
    };
    std::vector<Draw_info> draw_info_;
    const Resource_loader& loader_;
};

} // namespace fgs_demo
