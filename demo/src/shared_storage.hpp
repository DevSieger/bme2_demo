/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/basic_buffer.hpp>
#include "scene_data.hpp"
#include "binding_points.hpp"

namespace fgs_demo {


class Shared_storage
{
public:
    using Size = std::size_t;

    fgs::Buffer& ball_buffer()  { return balls_;};
    fgs::Buffer& pls_buffer()   { return pls_;};
    fgs::SSBO_base& pls_ssbo()  { return pls_ssbo_;}

    void init();

    void upload_balls(const Scene_data::Balls& data);
    void upload_pl(const Scene_data::Point_lights& data);

    Size num_balls()    const   { return n_balls_;};
    Size num_pls()      const   { return n_pls_;};
private:
    fgs::Buffer     balls_      {GL_ARRAY_BUFFER,GL_STREAM_DRAW};
    fgs::Buffer     pls_        {GL_ARRAY_BUFFER,GL_STREAM_DRAW};
    fgs::SSBO_base  pls_ssbo_   { ssbo_binding_points::POINT_LS, pls_.storage() };

    Size n_balls_   = 0;
    Size n_pls_     = 0;

};


} // namespace fgs_demo

