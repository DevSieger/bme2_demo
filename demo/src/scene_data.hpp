/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include "camera.hpp"
#include <vector>

namespace fgs_demo {

using Vec3      = glm::vec3;
using Rotation  = glm::quat;

struct alignas(16) Ball{
    Vec3        pos;
    float       radius;
    Vec3        color;
    float       radiance;   // amount of emmited light
    Rotation    rotation;
};

struct alignas(16) Point_light{
    Vec3                pos;
    // to calculate an "angular" size
    float               radius;

    Vec3                illumination;
    // padding

    // x, y, z is parameters of attenuation that defines the attenuation function:
    // attenuation(d) = 1 / (x + y*d + z*d*d)
    alignas(16) Vec3    attenuation_p;
};

struct alignas(16) Directional_light{
    Vec3    direction;
    float   radius;         // angular size of the light source
    Vec3    illumination;
};

using Ambient_light = Vec3;

struct Scene_data{
    using Balls                 = std::vector<Ball>;
    using Point_lights          = std::vector<Point_light>;
    using Directional_lights    = std::vector<Directional_light>;

    Camera              cam;
    Balls               balls;
    Point_lights        point_lights;
    Directional_lights  directional_lights;
    Ambient_light       ambient_light;
    Vec3                sky_color;  // HDR

    struct{
        struct{
            // values of automatic tonemapping key deduction.
            // key = key_b *log(1 + key_a * average) + key_c
            // where average is geometric mean of luminance.
            // a key value defines brightness of the scene.
            // read "Erik Reinhard at al. , Photographic Tone Reproduction for Digital Images."
            float key_a = 0.19721736312181016487f;
            float key_b = 1;
            float key_c = 0;
        } tonemapping;
    } cfg;
};

inline Point_light make_point_light(const Vec3& pos, float radius, const Vec3& color, float target_distance){
    Point_light ret;
    ret.attenuation_p   = {1, 2/radius, 1/(radius*radius) };
    auto& a             = ret.attenuation_p;
    ret.pos             = pos;
    ret.radius          = radius;
    float ia            = a.x + a.y*target_distance + a.z * target_distance * target_distance;
    ret.illumination    = ia * color;
    return ret;
}

} // namespace fgs
