/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "camera.hpp"
#include <limits>
#include <glm/gtc/matrix_transform.hpp>

namespace fgs_demo {

View_position&View_position::direction(View_position::Direction new_dir, View_position::Direction new_up){
    new_dir = glm::normalize(new_dir);
    new_up  = glm::normalize(new_up);

    // is new_up = a * new_dir ?
    if( glm::abs(glm::dot(new_dir,new_up)) > 1-glm::epsilon<float>()*2){
        // if so, we have to choose another "up"
        if( new_dir.y*new_dir.y > std::numeric_limits<float>::min() ||
                new_dir.z*new_dir.z > std::numeric_limits<float>::min() )
        {
            new_up  = glm::vec3{ 1,0,0 };
        }else{
            new_up  = glm::vec3{ 0,0,1 };
        }
    }
    glm::mat3x3 tmp;
    tmp[0] = glm::normalize(glm::cross(new_dir,new_up));
    tmp[1] = new_dir;
    tmp[2] = glm::cross(tmp[0],tmp[1]);

    rot(glm::quat_cast(tmp));

    return *this;
}

Camera::Transform Camera::view_matrix() const
{
    static glm::mat4x4 adj{1,0,0,0, 0,0,-1,0, 0,1,0,0, 0,0,0,1 };
    glm::mat4x4 tmp = adj*glm::mat4_cast(glm::conjugate(view_position().rot()));
    return glm::translate(tmp, -view_position().pos());
}

Camera::Transform Camera::projection_matrix(float width, float height) const
{
    return glm::perspectiveFovRH_NO(fov(),width,height,nearest(),farthest());
}

} // namespace fgs_demo
