/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "misc.hpp"
#include <regex>
#include <chrono>

namespace fgs_demo {

std::string prepare_shader(const std::string& src){
    static std::regex version_regex{"#version"};
    std::string result;
    result.reserve(src.size()+2);
    std::regex_replace(std::back_inserter(result),src.begin(),src.end(),version_regex,"//$&");
    return result;
}

Rnd_seed get_seed(){
    std::random_device rd;
    using Clock = std::chrono::high_resolution_clock;
    using Ms    = std::chrono::duration<Rnd_seed,std::micro>;
    auto tp     = std::chrono::time_point_cast<Ms>(Clock::now());
    static_assert(std::is_unsigned<Rnd_seed>::value,"result type of random device is supposed to be unsigned");
    return rd() + tp.time_since_epoch().count();
}

Rnd_engine& rnd_engige(){
    static Rnd_engine engine(get_seed());
    return engine;
}

} // namespace fgs_d
