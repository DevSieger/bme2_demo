/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "drawers.hpp"
#include "scene_data.hpp"
#include <array>

namespace fgs_demo {

using namespace fgs;

namespace  {

const std::array<Vec3,42>& sphere_vertices(){
    static const std::array<Vec3,42> sphere_vertices_data
    {
        Vec3{0.000000, -1.000000, 0.000000},
        Vec3{0.723607, -0.447220, 0.525725},
        Vec3{-0.276388, -0.447220, 0.850649},
        Vec3{-0.894426, -0.447216, 0.000000},
        Vec3{-0.276388, -0.447220, -0.850649},
        Vec3{0.723607, -0.447220, -0.525725},
        Vec3{0.276388, 0.447220, 0.850649},
        Vec3{-0.723607, 0.447220, 0.525725},
        Vec3{-0.723607, 0.447220, -0.525725},
        Vec3{0.276388, 0.447220, -0.850649},
        Vec3{0.894426, 0.447216, 0.000000},
        Vec3{0.000000, 1.000000, 0.000000},
        Vec3{-0.162456, -0.850654, 0.499995},
        Vec3{0.425323, -0.850654, 0.309011},
        Vec3{0.262869, -0.525738, 0.809012},
        Vec3{0.850648, -0.525736, 0.000000},
        Vec3{0.425323, -0.850654, -0.309011},
        Vec3{-0.525730, -0.850652, 0.000000},
        Vec3{-0.688189, -0.525736, 0.499997},
        Vec3{-0.162456, -0.850654, -0.499995},
        Vec3{-0.688189, -0.525736, -0.499997},
        Vec3{0.262869, -0.525738, -0.809012},
        Vec3{0.951058, 0.000000, 0.309013},
        Vec3{0.951058, 0.000000, -0.309013},
        Vec3{0.000000, 0.000000, 1.000000},
        Vec3{0.587786, 0.000000, 0.809017},
        Vec3{-0.951058, 0.000000, 0.309013},
        Vec3{-0.587786, 0.000000, 0.809017},
        Vec3{-0.587786, 0.000000, -0.809017},
        Vec3{-0.951058, 0.000000, -0.309013},
        Vec3{0.587786, 0.000000, -0.809017},
        Vec3{0.000000, 0.000000, -1.000000},
        Vec3{0.688189, 0.525736, 0.499997},
        Vec3{-0.262869, 0.525738, 0.809012},
        Vec3{-0.850648, 0.525736, 0.000000},
        Vec3{-0.262869, 0.525738, -0.809012},
        Vec3{0.688189, 0.525736, -0.499997},
        Vec3{0.162456, 0.850654, 0.499995},
        Vec3{0.525730, 0.850652, 0.000000},
        Vec3{-0.425323, 0.850654, 0.309011},
        Vec3{-0.425323, 0.850654, -0.309011},
        Vec3{0.162456, 0.850654, -0.499995}
    };
    return sphere_vertices_data;
}

const std::array<unsigned char,240>& sphere_indices(){
    static const std::array<unsigned char,240> sphere_indices_data
    {
        0, 13, 12,
        1, 13, 15,
        0, 12, 17,
        0, 17, 19,
        0, 19, 16,
        1, 15, 22,
        2, 14, 24,
        3, 18, 26,
        4, 20, 28,
        5, 21, 30,
        1, 22, 25,
        2, 24, 27,
        3, 26, 29,
        4, 28, 31,
        5, 30, 23,
        6, 32, 37,
        7, 33, 39,
        8, 34, 40,
        9, 35, 41,
        10, 36, 38,
        38, 41, 11,
        38, 36, 41,
        36, 9, 41,
        41, 40, 11,
        41, 35, 40,
        35, 8, 40,
        40, 39, 11,
        40, 34, 39,
        34, 7, 39,
        39, 37, 11,
        39, 33, 37,
        33, 6, 37,
        37, 38, 11,
        37, 32, 38,
        32, 10, 38,
        23, 36, 10,
        23, 30, 36,
        30, 9, 36,
        31, 35, 9,
        31, 28, 35,
        28, 8, 35,
        29, 34, 8,
        29, 26, 34,
        26, 7, 34,
        27, 33, 7,
        27, 24, 33,
        24, 6, 33,
        25, 32, 6,
        25, 22, 32,
        22, 10, 32,
        30, 31, 9,
        30, 21, 31,
        21, 4, 31,
        28, 29, 8,
        28, 20, 29,
        20, 3, 29,
        26, 27, 7,
        26, 18, 27,
        18, 2, 27,
        24, 25, 6,
        24, 14, 25,
        14, 1, 25,
        22, 23, 10,
        22, 15, 23,
        15, 5, 23,
        16, 21, 5,
        16, 19, 21,
        19, 4, 21,
        19, 20, 4,
        19, 17, 20,
        17, 3, 20,
        17, 18, 3,
        17, 12, 18,
        12, 2, 18,
        15, 16, 5,
        15, 13, 16,
        13, 0, 16,
        12, 14, 2,
        12, 13, 14,
        13, 1, 14
    };
    return sphere_indices_data;
}

}

//======== Full_screen_drawer ========//
void Full_screen_drawer::init()
{
    VAO::Binding bnd(vao_);
    check_gl_error();
    auto& vbo = vbo_;
    vbo.bind();
    Vertex_format vf;
    vf.add_f(0,2,GL_FLOAT,0);
    vf.set_stride(8);
    vf.apply(vbo.id(),0);
    float vd[] = {-1,3,-1,-1,3,-1};
    glBufferData(GL_ARRAY_BUFFER,sizeof(vd),vd,GL_STATIC_DRAW);
    check_gl_error();
}

void Full_screen_drawer::draw()
{
    VAO::Binding bnd(vao_);
    glDrawArrays(GL_TRIANGLES,0,3);
    check_gl_error();
}

//======== Ball_drawer ========//
Ball_drawer::Ball_drawer(Shared_storage& st):
    storage_(st)
{
}

void Ball_drawer::init()
{
    VAO::Binding bnd(vao_);
    check_gl_error();

    //vertices
    {
        Vertex_format vf;
        vf.add_f(0,3,GL_FLOAT,0);
        vf.set_stride(sizeof(Vec3));

        vf.apply(vbo_.id(),0);

        vbo_.bind();
        glBufferData(GL_ARRAY_BUFFER, sphere_vertices().size()*sizeof(Vec3),sphere_vertices().data(),GL_STATIC_DRAW);
        check_gl_error();
    }

    //indices
    {
        ibo_.bind();
        glBufferData(ibo_.type(), sphere_indices().size()*sizeof(unsigned char),sphere_indices().data(),GL_STATIC_DRAW);
        check_gl_error();
    }

    //instance data
    {
        Vertex_format vfi;

        vfi.add_f(1,3,GL_FLOAT,offsetof(Ball,pos));
        vfi.add_f(2,1,GL_FLOAT,offsetof(Ball,radius));
        vfi.add_f(3,3,GL_FLOAT,offsetof(Ball,color));
        vfi.add_f(4,1,GL_FLOAT,offsetof(Ball,radiance));
        vfi.add_f(5,4,GL_FLOAT,offsetof(Ball,rotation));
        vfi.set_stride<Ball>();
        vfi.apply(instance_buffer().id(),1);
    }
}

void Ball_drawer::draw(bool tesselation)
{
    VAO::Binding binding(vao_);
    if(num_instances()){
        GLenum mode = (tesselation)?GL_PATCHES:GL_TRIANGLES;
        glDrawElementsInstanced( mode, sphere_indices().size(),GL_UNSIGNED_BYTE,0,num_instances());
        check_gl_error();
    }
}

float Ball_drawer::inner_radius()
{
    auto&   v           = sphere_vertices();
    auto&   ix          = sphere_indices();
    float   res         = 1;

    // in practice single triangle is not enought,
    // we have to check ALL of them.
    for(unsigned int i = 0; i< sphere_indices().size(); i+=3){
        glm::dvec3 dv[3]    = {v[ix[i]],v[ix[i+1]],v[ix[i+2]]};
        glm::dvec3  n       = glm::normalize(glm::cross(dv[1]-dv[0],dv[2]-dv[0]));
        float tmp           = glm::dot(dv[0],n);
        res                 = glm::min(tmp,res);
    }
    return res;
}

unsigned Ball_drawer::num_instances() const
{
    return storage_.num_balls();
}

Buffer& Ball_drawer::instance_buffer()
{
    return storage_.ball_buffer();
}

//======== Point_light_drawer ========//

Point_light_drawer::Point_light_drawer(Shared_storage& st):
    storage_(st)
{
}

void Point_light_drawer::init()
{
    VAO::Binding bnd(vao_);
    check_gl_error();

    //vertices
    {
        Vertex_format vf;
        vf.add_f(0,3,GL_FLOAT,0);
        vf.set_stride(sizeof(Vec3));

        vf.apply(vbo_.id(),0);

        vbo_.bind();
        glBufferData(GL_ARRAY_BUFFER, sphere_vertices().size()*sizeof(Vec3),sphere_vertices().data(),GL_STATIC_DRAW);
        check_gl_error();
    }

    //indices
    {
        ibo_.bind();
        glBufferData(ibo_.type(), sphere_indices().size()*sizeof(unsigned char),sphere_indices().data(),GL_STATIC_DRAW);
        check_gl_error();
    }

    //instance data
    {
        Vertex_format vfi;

        vfi.add_f(1,3,GL_FLOAT,offsetof(Point_light,pos));
        vfi.add_f(2,1,GL_FLOAT,offsetof(Point_light,radius));
        vfi.add_f(3,3,GL_FLOAT,offsetof(Point_light,illumination));
        vfi.add_f(4,3,GL_FLOAT,offsetof(Point_light,attenuation_p));
        vfi.set_stride<Point_light>();
        vfi.apply(instance_buffer().id(),1);
    }
}

void Point_light_drawer::draw(GLuint start, GLsizei count)
{
    VAO::Binding binding(vao_);
    if(num_instances() > start){
        count = glm::min(count+start, num_instances()) - start;
        glDrawElementsInstancedBaseInstance(GL_TRIANGLES, sphere_indices().size() ,GL_UNSIGNED_BYTE,0,count,start);
        check_gl_error();
    }
}

float Point_light_drawer::inner_radius()
{
    return Ball_drawer::inner_radius();
}

unsigned Point_light_drawer::num_instances() const
{
    return storage_.num_pls();
}

Buffer& Point_light_drawer::instance_buffer()
{
    return storage_.pls_buffer();
}

//======== Dl_shadow_drawer ========//

namespace {

constexpr unsigned n_circle_edges = 16;

std::vector<Vec3> build_cylinder(){
    std::vector<Vec3> ret(n_circle_edges*2+2);
    unsigned ix = 0;
    for(unsigned z = 0; z<2; ++z){
        ret[ ix++ ] = Vec3{0,0,z};
        for(unsigned e = 0; e < n_circle_edges;++e,++ix){
            float angle = (glm::pi<float>()*2*e)/n_circle_edges;
            ret[ix].x   = glm::cos(angle);
            ret[ix].y   = glm::sin(angle);
            ret[ix].z   = z;
        }
    }
    return ret;
};

std::vector<unsigned char> build_cylinder_indices(){
    std::vector<unsigned char> ret(n_circle_edges*3*4);

    const unsigned second_size_start = n_circle_edges + 1;

    for(unsigned e = 0; e < n_circle_edges; ++e){
        unsigned ix     = e*12;
        unsigned ce     = e + 1;
        unsigned ne     = (e + 1) % n_circle_edges + 1;
        unsigned ce2    = second_size_start + ce;
        unsigned ne2    = second_size_start + ne;

        ret[ix]     = 0;
        ret[ix+1]   = ne;
        ret[ix+2]   = ce;

        ret[ix+3]   = ce2;
        ret[ix+4]   = ce;
        ret[ix+5]   = ne;

        ret[ix+6]   = ce2;
        ret[ix+7]   = ne;
        ret[ix+8]   = ne2;

        ret[ix+9]   = second_size_start;
        ret[ix+10]  = ce2;
        ret[ix+11]  = ne2;
    }

    return ret;
}

auto& get_cylinder_indices(){
    static const std::vector<unsigned char> res = build_cylinder_indices();
    return  res;
}

}

Dl_shadow_drawer::Dl_shadow_drawer(Shared_storage& st):
    storage_(st)
{}

void Dl_shadow_drawer::init()
{
    VAO::Binding bnd(vao_);
    check_gl_error();

    //vertices
    {
        Vertex_format vf;
        vf.add_f(0,3,GL_FLOAT,0);
        vf.set_stride(sizeof(Vec3));

        vf.apply(vbo_.id(),0);

        vbo_.bind();
        auto vertices = build_cylinder();
        glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(Vec3),vertices.data(),GL_STATIC_DRAW);
        check_gl_error();
    }

    //indices
    {
        ibo_.bind();
        glBufferData(ibo_.type(), get_cylinder_indices().size()*sizeof(unsigned char),get_cylinder_indices().data(),GL_STATIC_DRAW);
        check_gl_error();
    }

    //instance data
    {
        Vertex_format vfi;

        vfi.add_f(1,3,GL_FLOAT,offsetof(Ball,pos));
        vfi.add_f(2,1,GL_FLOAT,offsetof(Ball,radius));
        vfi.set_stride<Ball>();
        vfi.apply(instance_buffer().id(),1);
    }
}

void Dl_shadow_drawer::draw()
{
    VAO::Binding binding(vao_);
    if(num_instances()){
        glDrawElementsInstanced( GL_TRIANGLES, get_cylinder_indices().size(),GL_UNSIGNED_BYTE,0,num_instances());
        check_gl_error();
    }
}

float Dl_shadow_drawer::inner_radius()
{
    return glm::cos(glm::pi<float>()/n_circle_edges);
}

unsigned Dl_shadow_drawer::num_instances() const
{
    return storage_.num_balls();
}

Buffer& Dl_shadow_drawer::instance_buffer()
{
    return storage_.ball_buffer();
}

} // namespace fgs_demo
