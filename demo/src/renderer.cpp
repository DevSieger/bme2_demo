/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "renderer.hpp"
#include <dk/format_string.hpp>
#include <dk/logging.hpp>
#include <iostream>
#include "misc.hpp"
#include <glm/gtc/vec1.hpp>
#include <cassert>
#include "resource_loader.hpp"

namespace fgs_demo {

using namespace fgs;

namespace  {

void load_texture(const Resource_loader& loader, fgs::Texture_2D& out,
                  const char* path, int components, GLenum format, int max_mip_levels)
{
    auto get_px_fmt = [](int components) -> GLenum {
        const GLenum aux[] = {GL_RED, GL_RG, GL_RGB, GL_RGBA};
        components = glm::clamp(components-1, 0, 3);
        return aux[components];
    };

    auto imgp = loader.load_image(path, components);
    if(imgp){
        int lvls = glm::min<int>(glm::log2<float>(glm::max<float>(imgp->width(), imgp->height()))+1, max_mip_levels);
        out.storage(imgp->width(), imgp->height(), lvls, format);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, imgp->width(), imgp->height(), get_px_fmt(components), GL_UNSIGNED_BYTE, imgp->get());
        check_gl_error();
    }
}

namespace section {

// see a body of build_names() for description
enum: unsigned{
    UPLOAD,
    BALLS,
    GROUND,
    SKY,
    AMBIENT,
    PLS,
    DLS,
    LUM_MEAN,
    TONEMAPPING,
    CLEAR,
    TOTAL
};

namespace dtl {
using Names = std::array<std::string_view,TOTAL>;

constexpr Names build_names(){
    Names names;
    names[UPLOAD]       = "data upload";
    names[BALLS]        = "g-buffer: balls";
    names[GROUND]       = "g-buffer: ground";
    names[SKY]          = "a-buffer: sky";
    names[AMBIENT]      = "a-buffer: ambient light";
    names[PLS]          = "a-buffer: point light sources";
    names[DLS]          = "a-buffer: directional light sources";
    names[LUM_MEAN]     = "luminance geometric mean";
    names[TONEMAPPING]  = "tonemapping";
    names[CLEAR]        = "fbo clearing";
    return names;
};

constexpr bool check_names(const Names& names){
    for(auto& sv: names){
        if(sv.empty()) return false;
    }
    return true;
}
} // namespace dtl

constexpr dtl::Names names = dtl::build_names();
static_assert ( dtl::check_names(names), "not all section names have been defined" );

} // namespace section

// to avoid redundant code
#define OGL_SECTION(name) auto section_guard = section_start(section::name);

}

void Renderer::init(const Resource_loader& loader, unsigned width, unsigned height)
{
    std::cout << dk::fstr("init %1% x %2%",width,height)<<std::endl;

    perf_counters_.clear();
    for(auto& name: section::names){
        perf_counters_.emplace_back(name);
    }

    width_  = width;
    height_ = height;

    init_fbs();
    init_shaders(loader);
    init_textures(loader);
    init_cs_tx();

    //init drawers
    fsd_.init();
    ball_drwr_.init();
    pl_drwr_.init();
    dl_sv_drwr_.init();

    // length property of ssbo "[]" arrays depends on the size of an allocated memory in the buffer.
    // alignment breaks this.
    dls_ssbo_.storage().size_align(1);
    clear();

    sc_cfg_ubo_.commit();
}

void Renderer::upload_data(const Scene_data& sc, float dt)
{
    OGL_SECTION(UPLOAD);
    cam_ubo_.prefix_data().view     = sc.cam.view_matrix();
    cam_ubo_.prefix_data().proj     = sc.cam.projection_matrix(width_,height_);
    cam_ubo_.prefix_data().inv_proj = glm::inverse(cam_ubo_.prefix_data().proj);
    cam_ubo_.commit();
    storage_.upload_balls(sc.balls);
    storage_.upload_pl(sc.point_lights);

    {
        auto& dlss = dls_ssbo_.array_data();
        dlss.clear();
        dlss.reserve(sc.directional_lights.size());

        // note: dl is a copy, not reference.
        for(auto dl: sc.directional_lights){
            dl.direction    = glm::normalize(cam_ubo_.prefix_data().view * glm::vec4(dl.direction,0));
            dlss.push_back(dl);
        }

        dls_ssbo_.prefix_data().ambient_intensity   = sc.ambient_light;
        dls_ssbo_.prefix_data().sky_color           = sc.sky_color;
        dls_ssbo_.commit();
    }

    // TODO: sc_cfg_ubo_ uploading
    auto& cfg_data      = sc_cfg_ubo_.prefix_data();

    cfg_data.tm_key_a   = sc.cfg.tonemapping.key_a;
    cfg_data.tm_key_b   = sc.cfg.tonemapping.key_b;
    cfg_data.tm_key_c   = sc.cfg.tonemapping.key_c;

    cfg_data.dt         = (frames_done_ > 4)? dt : std::numeric_limits<float>::max();
    sc_cfg_ubo_.commit();
}

void Renderer::gbuffer_stencil_test(GB_stencil_mode mode)
{
    switch (mode) {
        case GB_stencil_mode::NONE:{
                    glDisable(GL_STENCIL_TEST);
        };break;
        case GB_stencil_mode::ENABLED:{
                    glEnable(GL_STENCIL_TEST);
                    glStencilFunc(GL_LESS,0,0xff);
                    glStencilOp(GL_KEEP,GL_KEEP,GL_KEEP);
        };break;
        case GB_stencil_mode::INVERTED:{
                    glEnable(GL_STENCIL_TEST);
                    glStencilFunc(GL_EQUAL,0,0xff);
                    glStencilOp(GL_KEEP,GL_KEEP,GL_KEEP);
        };break;
        case GB_stencil_mode::WRITE:{
                    glEnable(GL_STENCIL_TEST);
                    glStencilFunc(GL_ALWAYS,1,0xff);
                    glStencilOp(GL_KEEP,GL_KEEP,GL_REPLACE);
        };break;
    }
    check_gl_error();
}

void Renderer::geometry_pass()
{
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    check_gl_error();

    {
        OGL_SECTION(BALLS);
        ball_shader().use();
        gbuffer_.bind();

        gbuffer_stencil_test(GB_stencil_mode::WRITE);

        if(wire_mode()){
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }

        ball_drwr_.draw( ball_draw_mode()==Ball_draw_mode::TESSELATION );

        if(wire_mode()){
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }
    {
        OGL_SECTION(GROUND);
        shdr_.ground.use();
        fsd_.draw();
        gbuffer_stencil_test(GB_stencil_mode::NONE);
    }
}

void Renderer::light_stage()
{
    glDepthMask(GL_FALSE);
    glEnable(GL_DEPTH_TEST);
    check_gl_error();
    glEnable(GL_BLEND);
    glBlendEquationSeparate(GL_FUNC_ADD,GL_FUNC_ADD);

    auto set_light_blending = [] (){
        glBlendFuncSeparate(GL_ONE,GL_ONE,GL_ONE,GL_ZERO);
        check_gl_error();
    };
    auto set_shadow_blending = [] (){
        glBlendFuncSeparate(GL_DST_COLOR,GL_ZERO, GL_DST_ALPHA,GL_ZERO);
        check_gl_error();
    };

    set_light_blending();
    check_gl_error();
    {
        OGL_SECTION(SKY);
        abuffer_.bind();
        shdr_.sky.use();
        gbuffer_stencil_test(GB_stencil_mode::INVERTED);
        fsd_.draw();
    }

    // ambient light
    {
        OGL_SECTION(AMBIENT);

        gbuffer_stencil_test(GB_stencil_mode::ENABLED);
        shdr_.ambient_dls.use();
        fsd_.draw();
    }

    {
        OGL_SECTION(PLS);

        glCullFace(GL_FRONT);
        glDepthFunc(GL_GEQUAL);
        glEnable(GL_DEPTH_CLAMP);

        for(GLuint ix = 0; ix < storage_.num_pls(); ix += 4){
            set_shadow_blending();

            check_gl_error();
            fbo_shadow_dl_.bind();

            const float one[]    = {1,1,1,1};
            glClearBufferfv(GL_COLOR,0,one);

            auto last = glm::min<std::size_t>(ix+4,storage_.num_pls());
            shdr_.pl_shdw.use();

            for(auto i = ix; i < last; ++i){
                shdr_.pl_shdw.set_uniform_u("ls_ix",i);
                dl_sv_drwr_.draw();
            }

            set_light_blending();
            check_gl_error();
            abuffer_.bind();
            shdr_.pls.use();

            for(auto i = ix; i < last; ++i){
                shdr_.pls.set_uniform_u("ls_ix",i);
                pl_drwr_.draw(i,1);
            }
        }

        glCullFace(GL_BACK);
        glDepthFunc(GL_LESS);
    }
    {
        OGL_SECTION(DLS);

        GLuint num_dls = static_cast<GLuint>(dls_ssbo_.array_data().size());
        for(GLuint ix = 0; ix < num_dls; ix += 4){
            set_shadow_blending();
            glCullFace(GL_FRONT);
            glDepthFunc(GL_GEQUAL);
            check_gl_error();
            fbo_shadow_dl_.bind();

            const float one[]    = {1,1,1,1};
            glClearBufferfv(GL_COLOR,0,one);

            auto last = glm::min(ix+4, num_dls);
            shdr_.dl_shdw.use();
            for(auto i = ix; i < last; ++i){
                shdr_.dl_shdw.set_uniform_u("ls_ix",i);
                dl_sv_drwr_.draw();
            }

            set_light_blending();
            glDepthFunc(GL_LESS);
            glCullFace(GL_BACK);
            check_gl_error();
            abuffer_.bind();
            shdr_.dl_intensity.use();
            shdr_.dl_intensity.set_uniform_u("first_ls",ix);
            shdr_.dl_intensity.set_uniform_u("last_ls",last);
            fsd_.draw();
        }
        gbuffer_stencil_test(GB_stencil_mode::NONE);
    }
    glDisable(GL_DEPTH_CLAMP);
    glDisable(GL_BLEND);
    glDepthMask(GL_TRUE);
}

void Renderer::calc_luminance_gmean()
{
    OGL_SECTION(LUM_MEAN);

    std::swap(luma_aux_.luma_gm_front, luma_aux_.luma_gm_back);

    luma_aux_.luma_gm_front->bind_to_texture_unit_once(tx_binding_points::TX_LUM_GMEAN);

    shdr_.luma_reduce.use();
    // an input is load_intensity() from gbuffer.
    luma_aux_.luma_reduce_aux.bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY);

    using glm::uvec2;
    using glm::ivec2;
    uvec2 num_groups = ivec2(width_-1,height_-1)/get_reduce_step_size() + ivec2(1);
    glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
    glDispatchCompute(num_groups.x,num_groups.y,1);

    shdr_.luma_reduce2.use();
    luma_aux_.luma_reduce_aux.bind_to_texture_unit_once(tx_binding_points::CS_INPUT_A);
    luma_aux_.luma_gm_back->bind_to_image_unit_once(img_binding_points::CS_OUTPUT_A,GL_WRITE_ONLY);
    glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
    glDispatchCompute(1,1,1);
}

void Renderer::final_pass()
{
    OGL_SECTION(TONEMAPPING);

    gbuffer_.bind(GL_READ_FRAMEBUFFER);
    Frame_buffer::default_fbo().bind(GL_DRAW_FRAMEBUFFER);
    glBlitFramebuffer(0,0,width_,height_,0,0,width_,height_, GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT,GL_NEAREST);

    glDepthMask(GL_FALSE);
    glDisable(GL_DEPTH_TEST);

    shdr_.tonemap.use();
    glEnable(GL_FRAMEBUFFER_SRGB);
    fsd_.draw();
    glDisable(GL_FRAMEBUFFER_SRGB);
}

void Renderer::clear()
{
    OGL_SECTION(CLEAR);
    gbuffer_.bind();
    glDepthMask(GL_TRUE);
    const float default_normal[]    = {0,0,1,0};
    const float default_albedo[]    = {0.1,0.2,0.5,0};
    const float zero[]              = {0,0,0,0};
    glClearBufferfv(GL_COLOR,0,default_albedo);
    glClearBufferfv(GL_COLOR,1,default_normal);
    glClearBufferfv(GL_COLOR,2,zero);   //specular color
    glClearBufferfi(GL_DEPTH_STENCIL,0,1,0);

    abuffer_.bind();
    const float default_hdr[]    = {0,0,0,0};
    glClearBufferfv(GL_COLOR,0,default_hdr);
}

void Renderer::draw(const Scene_data& sc, float dt)
{
    if( last_scene_ != &sc ){
        frames_done_    = 0;
        last_scene_     = &sc;
    }

    glViewport(0, 0, width_, height_);
    glEnable(GL_CULL_FACE);
    upload_data(sc,dt);

    // start clearing default frame buffer
    {
        Frame_buffer::default_fbo().bind();
        glClearColor(0.1f,0.2f,0.5f,1);
        glDepthMask(GL_TRUE);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    geometry_pass();
    light_stage();
    calc_luminance_gmean();
    final_pass();

    // let it clear at an idle time.
    clear();

    if( frames_done_ < max_frames_to_count){
        ++frames_done_;
    }
    update_stat(dt);
}

void Renderer::init_fbs()
{
    rt_.albedo.     storage(width_,height_,1,GL_RGB10_A2);
    rt_.spec.       storage(width_,height_,1,GL_RGB10_A2);
    rt_.depth.      storage(width_,height_,1, GL_DEPTH24_STENCIL8);
    rt_.hdr_color.  storage(width_,height_,1,GL_RGBA16F);
    rt_.normal.     storage(width_,height_,1,GL_RG16);
    rt_.shadow_tmp_dl.storage(width_,height_, 1, GL_RGBA8);

    gbuffer_.attach_to_depth_stencil(rt_.depth);
    gbuffer_.attach_to_color(rt_.albedo,0);
    gbuffer_.attach_to_color(rt_.normal,1);
    gbuffer_.attach_to_color(rt_.spec,2);
    gbuffer_.map_attachments(0,1,2);
    gbuffer_.check();

    abuffer_.attach_to_depth_stencil(rt_.depth);
    abuffer_.attach_to_color(rt_.hdr_color,0);
    abuffer_.map_attachments(0);
    abuffer_.check();

    fbo_shadow_dl_.attach_to_color(rt_.shadow_tmp_dl,0);
    fbo_shadow_dl_.attach_to_depth_stencil(rt_.depth);
    fbo_shadow_dl_.map_attachments(0);
    fbo_shadow_dl_.check();

    rt_.albedo.     bind_to_texture_unit(tx_binding_points::RT_ALBEDO);
    rt_.depth.      bind_to_texture_unit(tx_binding_points::RT_DEPTH);
    rt_.hdr_color.  bind_to_texture_unit(tx_binding_points::RT_HDR_COLOR);
    rt_.normal.     bind_to_texture_unit(tx_binding_points::RT_NORMAL);
    rt_.spec.       bind_to_texture_unit(tx_binding_points::RT_SPEC);
    rt_.shadow_tmp_dl.bind_to_texture_unit(tx_binding_points::TX_SHADOW_TMP_DL);
}

void Renderer::init_cs_tx()
{
    using glm::ivec2;
    ivec2 reduced_size = (ivec2(width_,height_) - ivec2(1))/get_reduce_step_size() + ivec2(1);
    luma_aux_.luma_reduce_aux.storage(reduced_size.x, reduced_size.y, 1, GL_R32F);
}

void Renderer::init_shaders(const Resource_loader& rl)
{
    using Code = std::string;
    Code tmp;

    auto make_define_src = [] (std::string_view name, auto&& val){
        return dk::fstr("#define %1% %2%\n", name, std::forward<decltype(val)>(val) );
    };

    auto load_shader    = [&rl](std::string_view name){
        return prepare_shader(rl.load_shader_source(name));
    };

    Code ubo_src = make_define_src("SCENE_CFG_BP", ubo_binding_points::CFG);
    ubo_src += make_define_src("DLS_SSBO_BP", ssbo_binding_points::DIRECTIONAL_LS);
    ubo_src += make_define_src("CAM_UBO_BP", ubo_binding_points::CAMERA);
    ubo_src += make_define_src("PLS_SSBO_BP", ssbo_binding_points::POINT_LS);
    ubo_src += load_shader("ubo.glsl");

    Code lighting_src_fs    = load_shader("lighting_h.glsl");
    Code lighting_src_vs    = load_shader("lighting_vs_h.glsl");
    Code gbuffer_src        = load_shader("gbuffer.glsl");

    auto make_gbuffer = [&ubo_src, &gbuffer_src](GLenum type, std::string_view name, unsigned version) -> Shader{
        Shader sh(type,name,version);
        sh.add_source(ubo_src);
        sh.define("RT_ALBEDO_BP",   tx_binding_points::RT_ALBEDO);
        sh.define("RT_SPECULAR_BP", tx_binding_points::RT_SPEC);
        sh.define("RT_NORMAL_BP",   tx_binding_points::RT_NORMAL);
        sh.define("RT_HDR_COLOR_BP",tx_binding_points::RT_HDR_COLOR);
        sh.define("RT_DEPTH_BP",    tx_binding_points::RT_DEPTH);
        sh.define("TX_LUM_GMEAN_BP",tx_binding_points::TX_LUM_GMEAN);
        sh.add_source(gbuffer_src);
        sh.compile();
        return sh;
    };

    const unsigned glsl_version = 430;

    Shader gbuffer_fs = make_gbuffer(GL_FRAGMENT_SHADER,"gbuffer_fs",glsl_version);
    Shader gbuffer_vs = make_gbuffer(GL_VERTEX_SHADER,"gbuffer_vs",glsl_version);
    Shader gbuffer_cs = make_gbuffer(GL_COMPUTE_SHADER,"gbuffer_cs",glsl_version);

    {
        Shader vs(GL_VERTEX_SHADER,"vs_ball",glsl_version);
        vs.add_source(ubo_src);
        tmp = load_shader("vs_ball.glsl");
        vs.add_source(tmp);
        vs.compile();

        Shader tc(GL_TESS_CONTROL_SHADER,"tc_ball",glsl_version);
        tmp = load_shader("tc_ball.glsl");
        tc.add_source(tmp);
        tc.compile();

        Shader te(GL_TESS_EVALUATION_SHADER,"te_ball",glsl_version);
        te.add_source(ubo_src);
        tmp = load_shader("te_ball.glsl");
        te.add_source(tmp);
        te.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_ball",glsl_version);
        tmp = load_shader("fs_ball.glsl");
        fs.add_source(tmp);
        fs.compile();
        shdr_.balls_ts.attach(vs).attach(tc).attach(te).attach(fs).attach(gbuffer_fs).link();
        shdr_.balls_ts.set_uniform_i("albedo_tx",tx_binding_points::TX_BALL_ALBEDO);
    }
    {
        Shader vs(GL_VERTEX_SHADER,"vs_ball_rc",glsl_version);
        vs.add_source(ubo_src);
        vs.define("RADIUS_TUNE_FACTOR", 1.0f / Ball_drawer::inner_radius() );
        tmp = load_shader("vs_ball_rc.glsl");
        vs.add_source(tmp);
        vs.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_ball_rc",glsl_version);
        fs.add_source(ubo_src);
        tmp = load_shader("fs_ball_rc.glsl");
        fs.add_source(tmp);
        fs.compile();
        shdr_.balls_rc.attach(vs).attach(fs).attach(gbuffer_fs).link();
        shdr_.balls_rc.set_uniform_i("albedo_tx",tx_binding_points::TX_BALL_ALBEDO);
    }

    {
        Shader vs(GL_VERTEX_SHADER,"vs_ground",glsl_version);
        vs.add_source(ubo_src);
        tmp = load_shader("vs_ground.glsl");
        vs.add_source(tmp);
        vs.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_ground",glsl_version);
        tmp = load_shader("fs_ground.glsl");
        fs.add_source(tmp);
        fs.compile();
        shdr_.ground.attach(vs).attach(fs).attach(gbuffer_fs).link();
        shdr_.ground.set_uniform_i("ground_albedo_tx",tx_binding_points::TX_GRND_ALBEDO);
        shdr_.ground.set_uniform_i("ground_normal_tx",tx_binding_points::TX_GRND_NORMAL);
        shdr_.ground.set_uniform_i("ground_spec_tx",tx_binding_points::TX_GRND_SPEC);
    }

    {
        Shader vs(GL_VERTEX_SHADER,"vs_unit",glsl_version);
        tmp = load_shader("vs_unit.glsl");
        vs.add_source(tmp);
        vs.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_sky",glsl_version);
        fs.add_source(ubo_src);
        tmp = load_shader("fs_sky.glsl");
        fs.add_source(tmp);
        fs.compile();

        shdr_.sky.attach(vs).attach(fs).link();
    }

    {
        Shader vs(GL_VERTEX_SHADER,"vs_pls",glsl_version);
        vs.define("RADIUS_TUNE_FACTOR", 1.0f / Point_light_drawer::inner_radius() );
        vs.add_source(ubo_src).add_source(lighting_src_vs);
        tmp = load_shader("vs_pls.glsl");
        vs.add_source(tmp);
        vs.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_pls",glsl_version);
        fs.add_source(lighting_src_fs);
        tmp = load_shader("fs_pls.glsl");
        fs.add_source(tmp);
        fs.compile();

        shdr_.pls.attach(vs).attach(fs).attach(gbuffer_vs).attach(gbuffer_fs).link();
        shdr_.pls.set_uniform_i("tx_lum_gmean",tx_binding_points::TX_LUM_GMEAN);
        shdr_.pls.set_uniform_i("shadow_tx",tx_binding_points::TX_SHADOW_TMP_DL);
    }

    {
        Shader vs(GL_VERTEX_SHADER,"vs_dl_shadow",glsl_version);
        vs.add_source(ubo_src);
        vs.define("RADIUS_TUNE_FACTOR", 1.0f / Dl_shadow_drawer::inner_radius() );
        tmp = load_shader("vs_dl_shadow.glsl");
        vs.add_source(tmp);
        vs.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_dl_shadow",glsl_version);
        fs.add_source(lighting_src_fs);
        tmp = load_shader("fs_dl_shadow.glsl");
        fs.add_source(tmp);
        fs.compile();

        shdr_.dl_shdw.attach(vs).attach(fs).attach(gbuffer_fs).link();
    }

    {
        Shader vs(GL_VERTEX_SHADER,"vs_pl_shadow",glsl_version);
        vs.add_source(ubo_src).add_source(lighting_src_vs);
        vs.define("RADIUS_TUNE_FACTOR", 1.0f / Dl_shadow_drawer::inner_radius() );
        tmp = load_shader("vs_pl_shadow.glsl");
        vs.add_source(tmp);
        vs.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_pl_shadow",glsl_version);
        fs.add_source(lighting_src_fs);
        tmp = load_shader("fs_pl_shadow.glsl");
        fs.add_source(tmp);
        fs.compile();

        shdr_.pl_shdw.attach(vs).attach(fs).attach(gbuffer_vs).attach(gbuffer_fs).link();
        shdr_.pl_shdw.set_uniform_i("tx_lum_gmean",tx_binding_points::TX_LUM_GMEAN);
    }

    {
        Shader vs(GL_VERTEX_SHADER,"vs_unit",glsl_version);
        tmp = load_shader("vs_unit.glsl");
        vs.add_source(tmp);
        vs.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_dl_intensity",glsl_version);
        fs.add_source(ubo_src);
        fs.add_source(lighting_src_fs);
        tmp = load_shader("fs_dl_intensity.glsl");
        fs.add_source(tmp);
        fs.compile();
        shdr_.dl_intensity.attach(vs).attach(fs).attach(gbuffer_fs).link();
        shdr_.dl_intensity.set_uniform_i("shadow_tx",tx_binding_points::TX_SHADOW_TMP_DL);
    }

    {
        Shader vs(GL_VERTEX_SHADER,"vs_unit",glsl_version);
        tmp = load_shader("vs_unit.glsl");
        vs.add_source(tmp);
        vs.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_ambient",glsl_version);
        fs.add_source(ubo_src);
        fs.add_source(lighting_src_fs);
        tmp = load_shader("fs_ambient.glsl");
        fs.add_source(tmp);
        fs.compile();

        shdr_.ambient_dls.attach(vs).attach(fs).attach(gbuffer_fs).link();
    }

    {
        Shader vs(GL_VERTEX_SHADER,"vs_unit",glsl_version);
        tmp = load_shader("vs_unit.glsl");
        vs.add_source(tmp);
        vs.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_tonemapping",glsl_version);
        fs.add_source(ubo_src);
        tmp = load_shader("fs_tonemapping.glsl");
        fs.add_source(tmp);
        fs.compile();
        shdr_.tonemap.attach(vs).attach(fs).attach(gbuffer_fs).link();
        shdr_.tonemap.set_uniform_i("tx_lum_gmean",tx_binding_points::TX_LUM_GMEAN);
    }

    {
        Shader cs(GL_COMPUTE_SHADER,"cs_luma_reduce1",glsl_version);
        tmp = load_shader("cs_luma_reduce.glsl");
        cs.define("ILLUMINATION_INPUT",1);
        cs.add_source(tmp);
        cs.compile();

        Shader cs2(GL_COMPUTE_SHADER,"cs_luma_reduce2",glsl_version);
        cs2.add_source(ubo_src);
        cs2.define("ILLUMINATION_INPUT",0);
        cs2.define("LUMINANCE_G_MEAN_OUTPUT",1);
        tmp = load_shader("cs_luma_reduce.glsl");
        cs2.add_source(tmp);
        cs2.compile();

        shdr_.luma_reduce.attach(cs).attach(gbuffer_cs).link();
        shdr_.luma_reduce.set_uniform_i("img_out", img_binding_points::CS_OUTPUT_A);

        shdr_.luma_reduce2.attach(cs2).attach(gbuffer_cs).link();
        shdr_.luma_reduce2.set_uniform_i("img_in",tx_binding_points::CS_INPUT_A);
        shdr_.luma_reduce2.set_uniform_i("img_out",img_binding_points::CS_OUTPUT_A);
        shdr_.luma_reduce2.set_uniform_i("tx_lum_gmean",tx_binding_points::TX_LUM_GMEAN);

        GLint sz[3] = {0,0,0};
        glGetProgramiv(shdr_.luma_reduce.id(),GL_COMPUTE_LOCAL_WORK_SIZE,sz);
        cache_.reduction_size.x = sz[0];
        cache_.reduction_size.y = sz[1]*4;
        check_gl_error();
    }
    shaders_on_resize();
}

void Renderer::shaders_on_resize()
{
    const float tes_threshold = 16; // maximal size in pixels of an triange side
    shdr_.balls_ts.set_uniform_f("threshold_factor",width_*0.5f/tes_threshold, height_*0.5f/tes_threshold );
}

void Renderer::init_textures(const Resource_loader& loader)
{
    load_texture(loader, tx_.ground,"ground.png",3,GL_SRGB8,20);
    tx_.ground.update_mipmaps();
    tx_.ground.bind_to_texture_unit(tx_binding_points::TX_GRND_ALBEDO);
    tx_.ground.anisotropy(4);
    tx_.ground.filter(GL_LINEAR_MIPMAP_LINEAR,GL_LINEAR);

    load_texture(loader, tx_.ground_nm,"ground_nm.png",3,GL_RGB8,20);
    tx_.ground_nm.update_mipmaps();
    tx_.ground_nm.bind_to_texture_unit(tx_binding_points::TX_GRND_NORMAL);
    tx_.ground_nm.anisotropy(4);
    tx_.ground_nm.filter(GL_LINEAR_MIPMAP_LINEAR,GL_LINEAR);

    load_texture(loader, tx_.ground_spec,"ground_spec.png",3,GL_SRGB8,20);
    tx_.ground_spec.update_mipmaps();
    tx_.ground_spec.bind_to_texture_unit(tx_binding_points::TX_GRND_SPEC);
    tx_.ground_spec.anisotropy(4);
    tx_.ground_spec.filter(GL_LINEAR_MIPMAP_LINEAR,GL_LINEAR);

    load_texture(loader, tx_.ball_albedo,"ball.png",3,GL_SRGB8,20);
    tx_.ball_albedo.update_mipmaps();
    tx_.ball_albedo.bind_to_texture_unit(tx_binding_points::TX_BALL_ALBEDO);
    tx_.ball_albedo.anisotropy(4);
    tx_.ball_albedo.filter(GL_LINEAR_MIPMAP_LINEAR,GL_LINEAR);
    tx_.ball_albedo.bind();
    tx_.ball_albedo.set_parameter({
                    {GL_TEXTURE_WRAP_S,GL_MIRRORED_REPEAT},
                    {GL_TEXTURE_WRAP_T,GL_MIRRORED_REPEAT}
    });

    // persistent aux stuff
    luma_aux_.luma_gmean_src[0].storage(1,1,1,GL_R32F);
    luma_aux_.luma_gmean_src[1].storage(1,1,1,GL_R32F);

    // to ensure that there is no NaN values.
    const GLfloat def_luma    = 1;
    for(int i = 0; i < 2; ++i){
        luma_aux_.luma_gmean_src[i].bind();
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 1, 1, GL_RED, GL_FLOAT, &def_luma);
    }
}

Shader_program& Renderer::ball_shader()
{
    switch (cfg_.ball_mode) {
        case Ball_draw_mode::RECONSTRUCTION:    return shdr_.balls_rc;
        case Ball_draw_mode::TESSELATION:       return shdr_.balls_ts;
        default: return shdr_.balls_rc;
    }
}

glm::ivec2 Renderer::get_reduce_step_size()
{
    return cache_.reduction_size;
}

Performance_counter::Section Renderer::section_start(unsigned ix)
{
    assert(ix < section::TOTAL && ix < perf_counters_.size());
    return Performance_counter::Section(perf_counters_[ix], cfg_.enable_perf_measure);
}

void Renderer::update_stat(double dt)
{
    auto& stat = cache_.stat_;
    if(cfg_.enable_perf_measure){
        // Performance_counter::get() returns a mean time for the PREVIOUS frame.
        // so here is a check for the previous frame
        if( cache_.stat_elapsed_ >= cfg_.stat_update_interval ){
            stat.clear();
            for(auto& cntr: perf_counters_){
                stat.emplace_back(Section_stat_entry{cntr.name(),cntr.get()});
                cntr.clear();
            }
            cache_.stat_elapsed_ = 0;
        }
        cache_.stat_elapsed_ += dt;
    }
}

const Renderer::Section_stat& Renderer::get_stat() const
{
    return cache_.stat_;
}

void Renderer::performance_counting(bool enabled)
{
    if(enabled != cfg_.enable_perf_measure){
        if(enabled){
            cache_.stat_elapsed_ = 0;
            for(auto& cntr: perf_counters_){
                cntr.clear();
            }
        }
        cache_.stat_.clear();
        cfg_.enable_perf_measure = enabled;
    }
}

void Renderer::resize(unsigned width, unsigned height)
{
    width_  = width;
    height_ = height;
    init_fbs();
    shaders_on_resize();
    init_cs_tx();
    clear();
}

} // namespace fgs_demo
