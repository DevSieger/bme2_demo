/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <string>
#include <string_view>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <vector>
#include <memory>

namespace fgs_demo {

class Font_info: public std::enable_shared_from_this<Font_info>{
public:
    using Size      = glm::uvec2;
    using Char_size = glm::uvec2;
    using Path      = std::string;
    using Char_ix   = unsigned char;

    const Path&         sprite_path() const {
        return path_;
    }
    const Char_size&    char_size() const {
        return char_size_;
    }
    const Size&         grid_size() const {
        return grid_size_;
    }
    // the index of the first character in the font
    unsigned            offset() const {
        return offset_;
    }

protected:
    // Font_info objects should be obtained by "user" using factory.
    Font_info(Path path, Size grid, Char_size  char_size_p, Char_ix first_char_index); // Path is copied any way
    ~Font_info() = default;

private:
    const Path      path_;
    const Size      grid_size_;
    const Char_size char_size_;
    const Char_ix   offset_;
};

using Font_ptr = std::shared_ptr<const Font_info>;

class Text_label
{
public:
    static constexpr char eol = '\n';
    using Position  = glm::ivec2;
    using String    = std::string;
    using Color     = glm::vec4;
    using Uint_size = glm::uvec2;

    struct Origin_point{
        enum Horizontal_mode{
            LEFT,
            CENTER,     // x-axis points right
            RIGHT
        };
        enum Vertical_mode{
            TOP,
            MIDDLE,     // y-axis points up
            BOTTOM
        };
        Horizontal_mode x   {LEFT};
        Vertical_mode   y   {TOP};
    };
    enum class Align {
        LEFT,
        CENTER,
        RIGHT,
        JUSTIFY
    };

    Text_label(Font_ptr font_p):
        font_(std::move(font_p))
    {}

    const Font_info& font() const{
        return *font_;
    }

    const String&   text() const {
        return text_;
    }
    Text_label&     text(std::string_view p_text){
        text_ = p_text;
        return *this;
    }

    /// Returns a position of the text label origin in pixels relative to the origin of the window
    const Position& position() const{
        return pos_;
    }

    /// Sets a position of the text label origin in pixels relative to the origin of the window
    Text_label&     position(const Position& orig){
        pos_ = orig;
        return *this;
    }

    /// Returns a position of the given origin point of the text label relative to the specified origin point of the window
    Position position( const Origin_point& label_orig, const Position& window_size, const Origin_point& window_orig ) const;

    /// Returns a size of a text of the lebel in characters, i.e. the size of the longest line (x) and the number of lines (y)
    Uint_size text_size() const;

    /// Returns a size of the label area in pixels
    Position    size() const{
        return text_size() * font().char_size();
    }

    /// Sets an origin point of the text label.
    ///
    /// note: this position implicitly defines an align.
    Text_label&         origin(const Origin_point& orig){
        label_orig_ = orig;
        return *this;
    }
    /// Returns an origin point of the text label.
    const Origin_point&  origin() const{
        return label_orig_;
    }

    /// Sets an origin point of the window.
    Text_label&         window_origin(const Origin_point& orig){
        wnd_orig_ = orig;
        return *this;
    }
    /// Returns an origin point of the window.
    const Origin_point& window_origin() const{
        return wnd_orig_;
    }

    /// Sets a color of the text
    Text_label& color(const Color& p_color) {
        color_ = p_color;
        return *this;
    }
    /// Returns a color of the text
    const Color& color() const {
        return color_;
    }

    /// Returns an align of the text
    Align align() const {
        return align_;
    }

    /// Sets an align of the text
    Text_label& align(Align p_align){
        align_  = p_align;
        return *this;
    }

private:
    Font_ptr            font_;
    String              text_;
    Color               color_ {0,0,0,1};
    Position            pos_;
    Origin_point        label_orig_;
    Origin_point        wnd_orig_;
    Align               align_ {Align::LEFT};
};

struct GUI_data{
    std::vector<Text_label> labels;
};

} // namespace fgs_demo
