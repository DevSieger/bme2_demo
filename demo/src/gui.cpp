/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "gui.hpp"
#include <cstdio>
#include <dk/logging.hpp>

namespace fgs_demo {

namespace {
// to avoid store of unneccessary information
// indices of text labels in the gui data
namespace perf_labels {
enum{
    FPS,
    STAT,
    TOTAL
};
} // namespace perf_labels

constexpr const char* font_15 = "dejavu_mono_bold_15.png";
constexpr const char* font_mono_15 = "dejavu_mono_bold_15.png";
}

Performace_gui::Performace_gui(double decay_value):
    decay_(decay_value)
{}

void Performace_gui::build(const Font_factory& font_factory)
{
    auto& labels = data().labels;
    Font_ptr font = font_factory(font_15);
    labels.reserve(perf_labels::TOTAL);

    Text_label lbl(font);
    using Orig = Text_label::Origin_point;
    Orig worig; // the origin point of the window
    Orig orig;  // the origin of the label
    static_assert ( perf_labels::FPS == 0);
    worig.x = Orig::RIGHT;
    worig.y = Orig::TOP;
    orig.x  = Orig::RIGHT;
    orig.y  = Orig::TOP;
    lbl.color({1,1,1,0.5}).window_origin(worig).origin(orig).position({16,16}).text("fps: 000");
    labels.push_back(lbl);

    static_assert ( perf_labels::STAT == 1);
    orig.y = worig.y = Orig::BOTTOM;
    orig.x = worig.x = Orig::LEFT;
    lbl.color({1,1,1,1}).window_origin(worig).origin(orig).position({16,32}).text("no statistic");
    labels.push_back(lbl);
}

void Performace_gui::update_performance(double dt)
{
    if( aft_ > 0 ){
        aft_ = aft_*decay_ + (1-decay_)*dt;
    }else{
        aft_ = dt;
    }
}

namespace {

std::string make_stat_string(const Performace_gui::Statistic& stat){
    using std::snprintf;

    // NRVO
    std::string res;

    if(stat.empty()){
        res = "waiting for the statistic data";
        return res;
    }

    // a header of the statistic table
    const char      header[]    = "the section statistic:\n\n";
    // the size of the header without null character
    const auto      header_sz   = sizeof (header) - 1;
    // precision of the printed value
    const int       precision   = 3;
    // format of the row
    const char      fmt[]       = "%-*.*s   %#*.*f us";
    // unit conversion from seconds to microseconds
    const double    time_conv   = 1000000;
    // format of the value, to determine the field width for the values
    const char      value_fmt[] = "%#.*f";
    // a width of the "name" field, in characters
    int field_name  = 0;
    // a width of the "value" field, in characters
    int field_value = 0;

    // determines a width of fields
    for(auto& entry: stat){
        if((std::size_t) field_name < entry.first.length()){
            field_name = entry.first.length();
        }
        int sz      = snprintf(nullptr,0,value_fmt, precision, entry.second*time_conv);
        if(field_value < sz) field_value = sz;
    }

    // includes the terminating null character
    int total_sz = header_sz;
    // calculates required memory size
    for(auto& entry: stat){
        auto& nm = entry.first;
        int sz      = snprintf(nullptr,0, fmt,
                               field_name, nm.size(), nm.data(),
                               field_value, precision, entry.second*time_conv);
        total_sz   += sz + 1;   // size of the line + the new line character
    }

    res.resize(total_sz,0);

    std::copy(header, header + header_sz, res.data());
    int cur_pos = header_sz;

    for(auto& entry: stat){
        auto& nm = entry.first;
        cur_pos += snprintf(res.data()+cur_pos, total_sz-cur_pos, fmt,
                            field_name, nm.size(), nm.data(),
                            field_value, precision, entry.second*time_conv);
        res[cur_pos] = '\n';
        ++cur_pos;
    }
    return res;
};

}

void Performace_gui::update([[maybe_unused]] double dt)
{
    auto& labels = data().labels;
    if(labels.size() == perf_labels::TOTAL){
        {
            const char fps_fmt[] = "fps: %4.2f";
            auto fps = get_fps();
            int fps_sz = std::snprintf(nullptr,0,fps_fmt,fps)+1;
            if(fps_sz ){
                // a vector here because of the terminating null character
                std::vector<char> mem(fps_sz,0);
                std::snprintf(mem.data(),fps_sz,fps_fmt,fps);
                labels[perf_labels::FPS].text(mem.data());
            }else{
                labels[perf_labels::FPS].text("error");
                dk::log(dk::Detail_level::NON_CRITICAL,"failed to print fps string");
            }
        }
        if(statistic_cb()){
            auto stat = make_stat_string(statistic_cb()());
            labels[perf_labels::STAT].text(stat);
        }
    }
}

double Performace_gui::get_fps() const
{
    return (aft_ > 0)? 1/aft_ : 0;
}

//======== Settings_gui ========//

void Settings_gui::build(const Font_factory& font_factory, std::string_view title, Settings settings_p)
{
    Font_ptr font = font_factory(font_mono_15);

    auto& lbls = data().labels;
    lbls.clear();
    settings_.swap(settings_p);
    selected_ = 0;
    unsigned max_name_len       = 0;
    unsigned max_opt_name_len   = 0;
    const unsigned spacing      = 8;

    for(auto& st: settings_){
        if( max_name_len < st.name().size() ){
            max_name_len = st.name().size();
        }
        for(Setting::Size ix = 0; ix < st.size(); ++ix){
            auto& opt = st.option_name(ix);
            if( max_opt_name_len < opt.size() ){
                max_opt_name_len = opt.size();
            }
        }
    }

    using Pos   = Text_label::Position;
    using OP    = Text_label::Origin_point;
    using Align = Text_label::Align;
    using Color = Text_label::Color;

    // total width of all labels exluding header
    const unsigned width    = (max_name_len + max_opt_name_len + spacing) * font->char_size().x;
    // total height
    const unsigned height   = settings_.size() * font->char_size().y;
    // current vertical position
    Pos::value_type cur_y   = height / 2;

    // x component of the origin point position for setting names
    const Pos::value_type names_x   = -(int)width / 2;
    // x component of the origin point position for option names
    const Pos::value_type opt_x     = width / 2 - (max_opt_name_len * font->char_size().x) / 2;

    Text_label name_proto(font), opt_proto(font);

    name_proto.window_origin({OP::CENTER,OP::MIDDLE}).origin({OP::LEFT,OP::TOP}).align(Align::LEFT);
    opt_proto.window_origin({OP::CENTER,OP::MIDDLE}).origin({OP::CENTER,OP::TOP}).align(Align::CENTER);

    {
        Text_label header(font);
        header.window_origin({OP::CENTER,OP::MIDDLE}).origin({OP::CENTER,OP::BOTTOM}).align(Align::CENTER)
                .position({0,cur_y+8}).color({1,1,1,0.85}).text(title);
        lbls.push_back(std::move(header));
    }
    for(std::size_t ix = 0; ix < settings_.size(); ++ix){
        const Color& color = ix == selected_? active_color() : inactive_color();
        auto& st = settings_[ix];

        lbls.push_back(name_proto);
        lbls.back().color(color).position({ names_x, cur_y }).text(st.name());

        lbls.push_back(opt_proto);
        lbls.back().color(color).position({ opt_x, cur_y }).text(st.option_name());

        cur_y -= font->char_size().y;
    }
}

bool Settings_gui::on_key(int key, [[maybe_unused]] int mods, fgs::SW_application::Action act)
{
    if(act == fgs::SW_application::Action::PRESS && !settings_.empty())
        switch (key) {
            case 'W': {
                select( ( selected_ - 1 + settings_.size()) % settings_.size() );
            }; return true;
            case 'S': {
                select( (selected_ + 1)  % settings_.size() );
            }; return true;
            case 'A': {
                change_value(selected_,-1);
            }; return true;
            case 'D': {
                change_value(selected_,1);
            }; return true;
        }
    else{
        switch (key) {
            case 'W':
            case 'S':
            case 'A':
            case 'D': return true;
        }
    }
    return false;
}

void Settings_gui::select(std::size_t ix)
{
    assert(ix < settings_.size());
    auto& lbls  = data().labels;
    auto sel_ix = name_label_ix(selected_);
    lbls[sel_ix].color(inactive_color());
    lbls[sel_ix+1].color(inactive_color());

    selected_ = ix;

    sel_ix = name_label_ix(selected_);
    lbls[sel_ix].color(active_color());
    lbls[sel_ix+1].color(active_color());
}

void Settings_gui::change_value(std::size_t ix, int delta)
{
    assert(ix < settings_.size());
    auto& lbl   = data().labels[name_label_ix(selected_) + 1];
    auto& st    = settings_[ix];
    auto nv     = ((st.active()+delta)+st.size()) % st.size();

    // we need to call this before st.activate
    lbl.text(st.option_name(nv));
    // "build" may be called on activation
    // this should be the last line of the method
    st.activate(nv);
}

std::size_t Settings_gui::name_label_ix(std::size_t setting_ix) const
{
    return setting_ix * 2 + 1;
}

const Settings_gui::Color& Settings_gui::active_color()
{
    static const Color value{1,1,1,1};
    return value;
}

const Settings_gui::Color& Settings_gui::inactive_color()
{
    static const Color value{0.95,0.95,0.95,0.5};
    return value;
}

void Help_screen::build(
        const Font_factory& font_factory,
        std::string_view header_text,
        std::string_view help_text)
{
    auto& lbls = data().labels;
    lbls.clear();
    using OP    = Text_label::Origin_point;

    Text_label header_lbl   (font_factory(font_15));
    Text_label help_lbl     (font_factory(font_mono_15));

    header_lbl.color({1,1,1,0.95})
            .window_origin({OP::CENTER,OP::MIDDLE})
            .origin({OP::CENTER,OP::TOP})
            .align(Text_label::Align::CENTER);

    help_lbl.color({1,1,1,0.95})
            .window_origin({OP::CENTER,OP::MIDDLE})
            .origin({OP::CENTER,OP::BOTTOM})
            .align(Text_label::Align::LEFT);

    lbls.push_back(std::move(header_lbl));
    lbls.push_back(std::move(help_lbl));

    text(header_text, help_text);
}

void Help_screen::text(std::string_view header_text, std::string_view help_text)
{
    if(data().labels.size() != 2) {
        dk::log(dk::Detail_level::NON_CRITICAL,
                "Help_screen::update_text() has been called before build()");
        return;
    }

    using Pos   = Text_label::Position;

    Text_label& header_lbl  = data().labels[0];
    Text_label& help_lbl    = data().labels[1];

    header_lbl.text(header_text);
    help_lbl.text(help_text);

    int padding         = 8;
    auto total_height   = help_lbl.size().y + padding + header_lbl.size().y;

    header_lbl  .position( Pos{0, total_height / 2});
    help_lbl    .position( Pos{0, -total_height / 2});
}

} // namespace fgs_demo
