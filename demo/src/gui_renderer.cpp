/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "gui_renderer.hpp"
#include "misc.hpp"
#include "binding_points.hpp"
#include <dk/logging.hpp>
#include <glm/common.hpp>
#include <algorithm>

namespace fgs_demo {

struct Instance_data{
    unsigned char   color[4];
    glm::vec2       origin;
    std::uint32_t   char_ix;
};

GUI_renderer::GUI_renderer(const Resource_loader& loader):
    loader_(loader)
{}

void GUI_renderer::init(unsigned width, unsigned height)
{
    using namespace fgs;

    {
        VAO::Binding guard(vao_);
        // VBO
        {
            unsigned char vertex_data[] = {
                0,0,        255,0,      255,255,
                255,255,    0,255,       0,0 };
            Vertex_format fmt;
            fmt.add_n(0,2,GL_UNSIGNED_BYTE,0);
            fmt.set_stride(2);
            fmt.apply(vbo_.id(),0);
            vbo_.bind();
            glBufferData(GL_ARRAY_BUFFER, sizeof (vertex_data), vertex_data, GL_STATIC_DRAW);
            check_gl_error();
        }
        // instances
        {
            Vertex_format fmt;
            fmt.add_n(1,4,GL_UNSIGNED_BYTE, offsetof(Instance_data,color));
            fmt.add_f(2,2,GL_FLOAT,         offsetof(Instance_data,origin));
            fmt.add_i(3,1,GL_UNSIGNED_INT,  offsetof(Instance_data,char_ix));
            fmt.set_stride<Instance_data>();
            fmt.apply(instances_.id(),1);
        }
    }

    const unsigned int version = 430;
    {
        Shader vs(GL_VERTEX_SHADER,"vs_text",version);
        std::string tmp = prepare_shader(loader_.load_shader_source("vs_text.glsl"));
        vs.add_source(tmp);
        vs.compile();

        Shader fs(GL_FRAGMENT_SHADER,"fs_text",version);
        tmp = prepare_shader(loader_.load_shader_source("fs_text.glsl"));
        fs.add_source(tmp);
        fs.compile();

        shader_.attach(vs).attach(fs).link();
        shader_.set_uniform_i("font_sprite",tx_binding_points::GUI_FONT_SPRITE);
    }
    resize(width, height);
    check_gl_error();
}

void GUI_renderer::resize(unsigned width, unsigned height)
{
    window_size_.x = width;
    window_size_.y = height;
    shader_.set_uniform_f("inv_wnd_size",1.0/width,1.0/height);
}

void GUI_renderer::draw()
{
    const Font* current = nullptr;

    // instance data is uploaded in the same order
    // it is possible to draw few labels with the single draw
    GLuint  base    = 0;
    GLsizei count   = 0;

    fgs::VAO::Binding guard(vao_);
    fgs::Frame_buffer::default_fbo().bind();
    glDepthMask(GL_FALSE);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_STENCIL_TEST);
    glEnable(GL_BLEND);
    glBlendEquationSeparate(GL_FUNC_ADD,GL_FUNC_ADD);
    glBlendFuncSeparate(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA,GL_ONE,GL_ZERO);
    check_gl_error();

    shader_.use();

    for(auto& di: draw_info_){
        if(current != di.font.get()){
            // draw accumulated instances
            if( count > 0 ){
                glDrawArraysInstancedBaseInstance(GL_TRIANGLES,0,6,count,base);
                base   += count;
                count   = 0;
                check_gl_error();
            }
            current = di.font.get();
            current->bind();
            shader_.set_uniform_u("start_index",current->offset());
            shader_.set_uniform_u("grid_size", current->grid_size().x, current->grid_size().y);
        }
        assert(di.start == count+base);
        count += di.count;
    }
    check_gl_error();
    if( count > 0 ){
        glDrawArraysInstancedBaseInstance(GL_TRIANGLES,0,6,count,base);
    }
    check_gl_error();
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    check_gl_error();
}

void GUI_renderer::upload(const GUI_data& gui, float dt)
{
    std::size_t total_chars = 0;
    for(auto& lbl: gui.labels){
        total_chars += lbl.text().size();
    }
    std::vector<Instance_data> data;
    data.reserve(total_chars);

    using glm::clamp;

    draw_info_.clear();
    draw_info_.reserve(gui.labels.size());

    for(auto& lbl: gui.labels){
        auto& text      = lbl.text();
        auto tx_size    = lbl.text_size();
        auto& font      = lbl.font();

        Draw_info di;
        {
            // it is safe to cast here.
            // it is possible to ensure safety, but it would be an overengineering.
            const Font* fnt = static_cast<const Font*>(&font);
            di.font         = std::shared_ptr<const Font>(font.shared_from_this(),fnt);
            di.start        = data.size();
        }

        auto label_size = lbl.size();

        using Orig = Text_label::Origin_point;
        // top left corner.
        auto tl_corner = lbl.position(Orig{Orig::LEFT,Orig::TOP},window_size_,Orig{Orig::LEFT,Orig::BOTTOM});

        Instance_data ch_data;

        // color is the same for all characters of the label
        for(unsigned i = 0 ;i<4;++i){
            ch_data.color[i] = clamp<unsigned>(lbl.color()[i] * 255, 0, 255);
        }

        {
            // from the top
            auto line_start = text.cbegin();
            const auto last = text.cend();

            // for non printable characters returns true.
            // non printable means out of the font :D
            // non printable characters is replaced by spaces
            auto is_space = [
                                first   = font.offset(),
                                last    = font.grid_size().x * font.grid_size().y + font.offset()
                            ] ( unsigned char ch_ix) -> bool {
                return ch_ix < first || ch_ix>=last;
            };

            for(unsigned line = 0; line < tx_size.y; ++line){
                const auto  line_end    = std::find(line_start,last,Text_label::eol);
                auto        line_sz     = (line_end - line_start)*font.char_size().x;

                // bottom left corner of the charcter to draw
                glm::vec2   cur_pos;   // floating point for the justify alignment

                // an additional size of the "space" characters
                // it is used for "JUSTIFY" alignment
                float   additional_space_size  = 0;

                using Align = Text_label::Align;
                switch ( unsigned free_space = label_size.x - line_sz; lbl.align() ) {
                    case Align::LEFT    : { cur_pos.x = tl_corner.x; };                     break;
                    case Align::CENTER  : { cur_pos.x = tl_corner.x + free_space / 2; };    break;
                    case Align::RIGHT   : { cur_pos.x = tl_corner.x + free_space; };        break;
                        // the ugly
                    case Align::JUSTIFY : {
                        cur_pos.x   = tl_corner.x;

                        // only spaces are enlarged
                        auto spaces = std::count_if(line_start, line_end,is_space);
                        if( spaces > 0 ) {
                            additional_space_size  = static_cast<float>(free_space) / spaces;
                        }
                    };  break;
                }

                cur_pos.y   = tl_corner.y - (line+1)*font.char_size().y;

                for(;line_start != line_end; ++line_start){
                    unsigned char ch_ix = *line_start;

                    if(is_space(ch_ix)){
                        cur_pos.x  += additional_space_size;
                    }else{
                        ch_data.char_ix  = ch_ix;
                        // avoid subpixel positions until an implementation of better font rendering
                        ch_data.origin   = glm::floor(cur_pos);
                        data.push_back(ch_data);
                    }
                    cur_pos.x += font.char_size().x;
                }

                if(line_start!=last) ++line_start; //skip eol
            }
        }

        di.count = data.size() - di.start;
        if(di.count) draw_info_.push_back(di);
    }

    instances_.storage().reallocate(data.size()*sizeof(Instance_data));
    instances_.storage().upload(0,data.data(),data.size());
}

Font_ptr GUI_renderer::load(const Font_info::Path& path, Font_info::Size grid, Font_info::Char_ix first_char_index)
{
    if(!grid.x || !grid.y){
        throw std::runtime_error(dk::fstr("invalid grid size: %1% x %2%",grid.x, grid.y));
    }

    auto img_ptr = loader_.load_image(path,1);

    if(!img_ptr){
        std::string what = dk::fstr("loading of the sprite font file %1% fails",path);
        dk::log(dk::Detail_level::CRITICAL, what);
        throw std::runtime_error(what);
    }

    fonts_.emplace_front(path, grid, first_char_index, img_ptr->width(), img_ptr->height(), img_ptr->get());
    return std::shared_ptr<Font_info>(&fonts_.front(), [this](Font_info* ptr){unload(ptr);});
}

void GUI_renderer::unload(Font_info* ptr)
{
    auto it     = fonts_.cbefore_begin();
    auto next   = fonts_.cbegin();
    while( next!=fonts_.cend() && static_cast<const Font_info*>(&*next)!=ptr ){
        it = next;
        ++next;
    }

    if( next!=fonts_.cend() ){
        fonts_.erase_after(it);
    }else{
        dk::log(dk::Detail_level::NON_CRITICAL,"could not find the font to unload: %1%", ptr->sprite_path());
    }
}

} // namespace fgs_demo
