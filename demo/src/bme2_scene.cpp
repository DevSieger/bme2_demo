/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "bme2_scene.hpp"
#include <bme2/system.hpp>
#include <bme2/ball.hpp>
#include <bme2/force.hpp>
#include <bme2/static_object.hpp>
#include "misc.hpp"
#include <random>
#include <glm/gtx/color_space.hpp>

namespace fgs_demo {
const double scale = 4;

struct Bme2_scene::D{
    bme2::System sys;
    std::vector<bme2::Ball> balls;
};

class Ground: public bme2::Static_object {
public:
    bool check_collision(
            const bme2::Vec3& origin,
            bme2::Dist radius,
            bme2::Vec3* normal_out,
            bme2::Vec3* correction_out) const override
    {
        if(origin.z - radius <= 0){
            *normal_out     = bme2::Vec3(0,0,1);
            *correction_out = bme2::Vec3(0,0,radius - origin.z);
            return true;
        }
        return false;
    }
};

class Border: public bme2::Static_object {
public:
    Border(bme2::Dist p_range = 32/scale): range_(p_range) {}

    bool check_collision(
            const bme2::Vec3& origin,
            bme2::Dist radius,
            bme2::Vec3* normal_out,
            bme2::Vec3* correction_out) const override
    {
        glm::dvec2 xy (origin.x, origin.y);
        const auto d2   = glm::dot(xy,xy);
        const bme2::Dist r  = range() - radius;
        if( d2 > r*r ){
            const auto dist = glm::sqrt(d2);
            *normal_out     = bme2::Vec3(xy / -dist , 0);
            *correction_out = *normal_out * ( dist - r);
            return true;
        }
        return false;
    }

    bme2::Dist range() const {
        return range_;
    }
    void range(bme2::Dist p_range){
        range_=p_range;
    }

private:
    bme2::Dist range_ {32};
};

class Simple_gravity: public bme2::Force {
public:
    bme2::Force_value get (const bme2::Ball& b, [[maybe_unused]] const bme2::Vec3 point, [[maybe_unused]] bool local) const override{
        return { bme2::Vec3(0,0,-9.8*b.mass()),bme2::Vec3{}};
    }
};

class Interaction: public bme2::Material_interaction{
public:
    Collision_parameters collision_parameters( [[maybe_unused]] const bme2::Ball& b1, [[maybe_unused]] const bme2::Ball& b2) override {
        return Collision_parameters{0.8, 0.6, 0.00008, 0.03};
    }

    Collision_parameters collision_parameters( [[maybe_unused]] const bme2::Ball& ball, [[maybe_unused]] const bme2::Static_object& st_obj) override{
        return Collision_parameters{0.9, 0.5, 0.0005, 0.01};
    }
};

void Bme2_scene::reset_cam()
{
    auto& vpos = data().cam.view_position();
    vpos.pos(View_position::Position{2,-19,3});
    vpos.direction(-vpos.pos());
}

void Bme2_scene::rebuild()
{
    m_ = std::make_unique<D>();

    // it is local to access private members of Bme2_scene
    class Heat_collision: public bme2::Collision_handler {
    public:
        Heat_collision(Bme2_scene& scene): sc_(scene) {}

    private:
        void update_ball(bme2::Ball& b, const bme2::Vec3& impulse, const bme2::Vec3& moment){
            std::size_t ix  = &b - sc_.d().balls.data();
            auto impact     = glm::length(impulse) + glm::length(moment);
            auto& gball     = sc_.data().balls[ix];
            gball.radiance += impact;
        }
        void on_collision(bme2::Ball& b1, bme2::Ball& b2, const bme2::Vec3& impulse_1, const bme2::Vec3& moment_1, const bme2::Vec3& moment_2) override {
            update_ball(b1, impulse_1, moment_1);
            update_ball(b2, -impulse_1, moment_2);
        };
        void on_collision(bme2::Ball& b1, bme2::Static_object& st, const bme2::Vec3& impulse, const bme2::Vec3& moment) override {
        };
        Bme2_scene& sc_;
    };

    auto& sc_balls  = data().balls;
    auto& ph_balls  = d().balls;
    auto& sc        = data();
    auto& sys       = d().sys;


    std::normal_distribution<double> velocity_dist(0,1);
    std::uniform_real_distribution<double> rad_dist(0.2,1);
    glm::uvec3 grid{4,4,4};

    unsigned ix = 0;
    float side  = glm::min(2u,grid.x*2);
    float step  = side/grid.x;
    float br    = step*0.25f;
    for(float z = br, lz = br + step*2*grid.z; z < lz; z+=step*2){
        for(float y = 0.5f*step*(1.0f - grid.y), ly = -y; y<=ly; y+=step){
            for(float x = 0.5f*step*(1.0f - grid.x), lx = -x; x <= lx; x+=step, ++ix){
                bme2::Ball_parameters x0;
                const double density = 500;
                x0.radius = step*0.25*rad_dist(rnd_engige());

                x0.mass = density*x0.radius*x0.radius*x0.radius;

                x0.pos  = bme2::Vec3{x,y,z};
                x0.vel  = bme2::Vec3{
                          velocity_dist(rnd_engige()),
                          velocity_dist(rnd_engige()),
                          velocity_dist(rnd_engige()) };
                x0.w    = bme2::Vec3{0,0,0};

                ph_balls.emplace_back(sys,x0);
            }
        }
    }

    sys.add(std::make_shared<Ground>());
    sys.add(std::make_shared<Border>());
    sys.apply(std::make_shared<Simple_gravity>(), bme2::Vec3{}, false);
    sys.make_material_interaction<Interaction>();
    sys.make_collision_handler<Heat_collision>(*this);

    sc_balls.resize(ph_balls.size());
    auto rnd_color = []() -> Vec3 {
        static std::uniform_real_distribution<float> h(210.0, 240.0);
        static std::uniform_real_distribution<float> s(0.0, 0.75);
        static std::uniform_real_distribution<float> v(0.75, 1);
        return glm::rgbColor(Vec3{h(rnd_engige()), s(rnd_engige()), v(rnd_engige())});
    };

    for(std::size_t ix = 0; ix < ph_balls.size(); ++ix){
        sc_balls[ix].pos = ph_balls[ix].position()*scale;
        sc_balls[ix].rotation = ph_balls[ix].rotation();
        sc_balls[ix].radius = ph_balls[ix].radius()*scale;
        sc_balls[ix].color  = rnd_color();
    }

    const float dl_scale    = 5;
    const float pl_scale    = 5;
    const float am_scale    = 5;
    const float sc_scale    = 8;

    sc.ambient_light    = Vec3{0.8,0.9,1} * am_scale;
    sc.sky_color        = Vec3{0.3,0.5,1} * sc_scale;

    Directional_light sun {Vec3(1,0,-8),glm::pi<float>()/15.0f,Vec3{1,0.95,0.85}*dl_scale};
    sc.directional_lights = {sun};

}

Bme2_scene::Bme2_scene()
{
    rebuild();
    reset_cam();
}

Bme2_scene::~Bme2_scene() = default;

void Bme2_scene::do_update(double dt)
{
    const float radiance_threshold = 0.0000001;
    if(m_){
        auto& sys = d().sys;

        const double it_time    = 0.0005;
        double remaining_t      = dt;
        while(remaining_t > 0){
            sys.iterate(glm::min(it_time, remaining_t));
            remaining_t -= it_time;
        }

        auto& sc_balls = data().balls;
        auto& ph_balls = d().balls;
        for(std::size_t ix = 0; ix < ph_balls.size(); ++ix){
            // hint: x = x * (1 - dt) is x(t) = exp(-t)
            auto& gball = sc_balls[ix];
            if( gball.radiance > radiance_threshold ) {
                gball.radiance = (gball.radiance > radiance_threshold) ? gball.radiance*(1 - dt) : 0;
            }
            gball.pos = ph_balls[ix].position()*scale;
            gball.rotation = ph_balls[ix].rotation();
        }
    }
}

bool Bme2_scene::on_key(int key, int mods, fgs::SW_application::Action act){
    if(key == ' ' && act == fgs::SW_application::Action::PRESS) {
        rebuild();
        return true;
    }
    return false;
}

} // namespace fgs_demo
