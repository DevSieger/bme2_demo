/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <vector>
#include <string>
#include <functional>
#include <stdexcept>

namespace fgs_demo {

class Setting {
public:
    using Name      = std::string;
    using Size      = std::size_t;
    using Action    = std::function<void(Size)>;

    /*!
     * \brief Constructs a settings object using parameters given
     * \param p_name        a name of the setting
     * \param p_action      a functor to call when a new option is activated. It takes an index of the option being activated as an argument.
     * \param option_names  a list of an option names, i-th name for i-th option, the size of the list determines the number of options
     * \param active_opt    the index of an option that is counted as active after initialization
     */
    Setting(Name p_name, Action p_action, std::initializer_list<Name> option_names, Size active_opt = 0):
        name_(std::move(p_name)), options_(option_names), action_(std::move(p_action)), current_(active_opt)
    {}

    /// Returns a name of the setting
    const Name& name() const{
        return name_;
    }

    /// Returns a name of the given option
    ///
    /// throws std::out_of_range if ix is greater than size()
    const Name& option_name(Size ix) const{
        return options_.at(ix);
    }

    /// Returns a name of the active option
    const Name& option_name() const{
        return option_name(active());
    }

    /// Returns the number of available options
    Size size() const{
        return options_.size();
    }

    /// Returns the active option
    Size active() const{
        return current_;
    }

    /*!
     * \brief Activates the option with the given index
     * \param ix    the given index
     *
     * If the given option is already active when the call is ignored.
     * If ix is greater than size(), std::out_of_range exception is thrown
     */
    void activate(Size ix){
        if(ix!=active()){
            if(ix >= size()){
                throw std::out_of_range("the option index is out of range");
            }
            current_ = ix;
            action()(ix);
        }
    }

    /// Returns a functor that is called when a new option is activated.
    const Action& action() const{
        return action_;
    }

private:
    const Name          name_;
    std::vector<Name>   options_;
    Action              action_;
    Size                current_    = 0;
};

using Settings = std::vector<Setting>;

} // namespace fgs_demo
