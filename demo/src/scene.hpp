/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include "scene_data.hpp"
#include "setting.hpp"
#include <fgs/sw_application.hpp>

namespace fgs_demo {

class Scene
{
public:
    Scene()             = default;
    virtual ~Scene()    = default;
    Scene(Scene&&)      = delete;

    const Scene_data& scene_data(){
        return sc_data_;
    }
    Camera& cam(){
        return sc_data_.cam;
    }

    void update(double dt);

    /// handles key press/release events, returns true if the event has been handled
    virtual bool on_key(
            [[maybe_unused]] int key,
            [[maybe_unused]] int mods,
            [[maybe_unused]] fgs::SW_application::Action act)
    { return false; };

    virtual Settings make_settings(){ return Settings{}; };

protected:
    Scene_data& data(){
        return sc_data_;
    }
private:
    virtual void do_update( [[maybe_unused]] double dt ){};
    Scene_data sc_data_;
};

class Test_scene : public Scene{
public:
    Test_scene();
private:
    void do_update(double dt) override;
    double time_ = 0;
};

class Benchmark_scene: public Scene{
public:
    Benchmark_scene();
    Settings make_settings() override;

private:
    using Grid = glm::uvec3;

    void build();
    void build_balls(Grid balls);
    void build_pls(unsigned n_pl);
    void do_update(double dt) override;

    double  time_ = 0;
    Grid    grd_;
};

class Snapshot_scene: public Scene{
public:
    Snapshot_scene( const Scene_data& sc){
        data() = sc;
    }
private:
};

} // namespace fgs_demo

