/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <memory>
#include <string>
#include <string_view>

namespace fgs_demo {

class Image_data {
public:
    // GLsizei is ... int
    using Size  = int;
    using Byte  = unsigned char;

    Size width()    const { return w_;}
    Size height()   const { return h_;}
    Size channels() const { return c_;}

    virtual Byte* get() const   = 0;
    virtual ~Image_data()       = default;

protected:
    Image_data(Size w, Size h, Size c):
        w_(w), h_(h), c_(c)
    {}

private:
    const Size w_, h_, c_;
};

class Resource_loader {
public:
    using Image_ptr     = std::unique_ptr<Image_data>;
    using Shader_source = std::string;

    virtual Shader_source   load_shader_source (std::string_view shader_name) const = 0;

    /*!
     * \brief Loads an image with the given resource name
     * \param image_name    the resource name of an image to load
     * \param desired_chnls the number of channels that the data should have
     *                          or 0 to use the number of channels in the target resource
     * \return a pointer to Image_data of the loaded image or an empty pointer if the loading fails,
     */
    virtual Image_ptr       load_image (std::string_view image_name, unsigned desired_chnls) const = 0;

    virtual ~Resource_loader() = default;
};



} // namespace fgs_demo

