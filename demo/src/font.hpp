/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/texture.hpp>
#include "gui_data.hpp"

namespace fgs_demo {

class Font: public Font_info{
public:
    /*!
     * \brief Constructs an object using the given image data
     * \param path      File path for Font_info
     * \param grid      the size of rows (x) and columns (y) of the font sprite in characters
     * \param first_char_index  an ASCII index of the first character in the font sprite
     * \param width     the width of the font sprite
     * \param height    the height of the font sprite
     * \param data      image data of the font sprite, it should be encoded using two 8-bit channels (gray + alpha) per pixel.
     */
    Font(const Path& path, Size grid, Char_ix first_char_index, int width, int height, unsigned char* data);
    void bind() const;
private:
    fgs::Texture_2D sprite_;
};

} // namespace fgs_demo

