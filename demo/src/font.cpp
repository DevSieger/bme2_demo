/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "font.hpp"
#include "binding_points.hpp"

namespace fgs_demo {

Font::Font(const Path& path, Size grid, Char_ix first_char_index, int width, int height, unsigned char* data):
    Font_info (path,grid, Char_size(width,height) / Char_size(grid), first_char_index)
{
    sprite_.bind();
    sprite_.storage(width,height,1,GL_R8);

    GLint old_align;
    glGetIntegerv(GL_UNPACK_ALIGNMENT,&old_align);
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);

    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RED, GL_UNSIGNED_BYTE, data);
    sprite_.set_parameter({
                               {GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE},
                               {GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE}
                           });

    glPixelStorei(GL_UNPACK_ALIGNMENT,old_align);
}

void Font::bind() const
{
    sprite_.bind_to_texture_unit_once(tx_binding_points::GUI_FONT_SPRITE);
}

} // namespace fgs_demo
