/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include "gui_data.hpp"
#include <fgs/sw_application.hpp>
#include <functional>
#include "setting.hpp"

namespace fgs_demo {

using Font_factory = std::function<Font_ptr(std::string_view sprite_path)>;

class GUI
{
public:
    const GUI_data& gui_data() const{
        return data_;
    }

    // updates GUI data
    // dt for an animation :D
    virtual void update( [[maybe_unused]] double dt ){};

    /// handles key press/release events, returns true if the event has been handled
    virtual bool on_key(
            [[maybe_unused]] int key,
            [[maybe_unused]] int mods,
            [[maybe_unused]] fgs::SW_application::Action act)
    { return false; };

    /// Returns false if the camera control is disabled (i.e. no input is provided)
    virtual bool camera_control_enabled() const { return true;}

    virtual ~GUI() = default;
protected:
    // the name differs from gui_data because of the different access level
    GUI_data& data() {
        return data_;
    }
private:
    GUI_data data_;
};

class Performace_gui : public GUI{
public:
    using Statistic_entry       = std::pair<std::string,double>;
    using Statistic             = std::vector<Statistic_entry>;

    // the return type is a reference to avoid redundant copying. it is safe here.
    using Statistic_cb  = std::function< const Statistic&() >;

    Performace_gui(double decay_value = 0.9 );
    void build(const Font_factory& font_factory);
    // to update fps, have to be called EVERY frame
    void update_performance(double dt);

    /// Returns a callback that provides a performance statistic of the rendering.
    const Statistic_cb& statistic_cb() const{
        return stat_cb_;
    }

    /// Sets a callback that provides a performance statistic of the rendering.
    ///
    /// empty callback disables an output of the statistic
    void statistic_cb(Statistic_cb cb){
        // rationale: to break dependence between GUI and Renderer
        // cb is passed by value, we need to copy anyway
        stat_cb_ = std::move(cb);
    }

    void update(double dt) override;
private:
    double get_fps() const;

    Statistic_cb stat_cb_;

    // "decay" value for fps calculation, from [0,1).
    // the higher values means smoother calculation
    double decay_;
    double aft_     = 0;
};

class Settings_gui: public GUI{
public:
    // settings_p is passed by value, we have to copy it anyway
    /// Initializes gui for the given settings
    void build(const Font_factory& font_factory, std::string_view title, Settings settings_p);

    bool on_key(int key, int mods, fgs::SW_application::Action act) override;

    bool camera_control_enabled() const override {
        return false;
    }

private:
    using Color = Text_label::Color;
    Settings settings_;
    unsigned selected_;

    /// Uses a setting with the given index as selected
    void select(std::size_t ix);
    /// Changes an option value for a setting with the given index by adding delta and wrapping around on overflow.
    void change_value(std::size_t ix, int delta);

    /// Returns an index of a label for the name of a setting with the given index.
    ///
    /// a label index for the name of an option for the given setting is the returned value + 1.
    std::size_t name_label_ix(std::size_t setting_ix) const;

    static const Color& active_color();
    static const Color& inactive_color();
};

class Help_screen : public GUI {
public:
    void build(const Font_factory& font_factory,
               std::string_view header_text,
               std::string_view help_text);

private:
    void text(std::string_view header_text, std::string_view help_text);
};

} // namespace fgs_demo

