/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include "camera.hpp"
#include <functional>

namespace fgs_demo {

class Camera_control
{
public:
    /// the functor that returns true if the key is pressed and false otherwise
    using Key_status_fn = std::function< bool(int key) >;

    /// Constructs a camera control for the given camera
    Camera_control(Camera& cam);

    Camera& camera(){
        return cam_;
    }
    const Camera& camera() const{
        return cam_;
    }

    /// Returns a movement velocity of the camera
    float velocity() const{
        return vel_;
    }

    /// Sets a movement velocity of the camera
    void velocity(float p_velocity_scale){
        vel_ = p_velocity_scale;
    }

    /*!
     * \brief Updates the state of the camera advancing dt seconds
     * \param dt    The time to advance
     * \param kf    The functor to get the state of keyboard keys. The empty state means input is disabled for this object.
     */
    virtual void update(float dt, const Key_status_fn& kf) = 0;

    virtual bool cursor_enabled() const{
        return true;
    }

    /*!
     * \brief Handles the mouse movement event
     * \param dx,dy the change of the cursor position
     * \return true if the event is handled
     *
     * The default implementation:
     * Calls rotate(dx,dy) if cursor_enabled() == false and returns true.
     * Returns false otherwise.
     */
    virtual bool on_mouse_move_delta(double dx, double dy);

    virtual ~Camera_control() = default;

private:
    virtual void rotate([[maybe_unused]] float dx, [[maybe_unused]] float dy){};

    Camera&     cam_;
    float       vel_{1};
};

class Point_watch: public Camera_control{
public:
    using Position = glm::vec3;

    /// Constructs an object that is managing the given camera
    Point_watch(Camera& cam);

    /// Constructs an object copying a state from the given Camera_control object
    Point_watch(const Camera_control& cctrl);

    const Position& point()const{
        return point_;
    }

    void point(const Position& p_point){
        point_=p_point;
    }

    void update(float dt, const Key_status_fn& kf) override;

private:
    Position point_ {0,0,0};
};

class First_person_view: public Camera_control{
public:
    /// Constructs an object that is managing the given camera
    First_person_view(Camera& cam);

    /// Constructs an object copying a state from the given Camera_control object
    First_person_view(const Camera_control& cctrl);

    void update(float dt, const Key_status_fn& kf) override;

    bool cursor_enabled() const override{
        return false;
    }

private:
    void rotate(float dx, float dy) override;
};

} // namespace fgs_demo

