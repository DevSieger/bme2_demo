/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "camera_control.hpp"

namespace fgs_demo {

Camera_control::Camera_control(Camera& cam):
    cam_(cam)
{}

namespace {

// dir is the direction of the camera movement in the local basis of the camera,
// {1,0,0} is the right direction, {0,1,0} is forward direction, {0,0,1} is the up direction.
View_position::Position generic_movement(const Camera_control& cctrl, float dt, const View_position::Direction& dir){
    auto& vpos      = cctrl.camera().view_position();

    View_position::Position res = vpos.pos();

    if(float len    = glm::length(dir)){
        res += (cctrl.velocity()*dt/len)*(vpos.rot()*dir);
    }
    return res;
}

View_position::Direction get_generic_direction(const Camera_control::Key_status_fn& key_reporter){
    View_position::Direction dir(0,0,0);
    if(key_reporter){
        if(key_reporter('W')) dir.y += 1;
        if(key_reporter('S')) dir.y -= 1;
        if(key_reporter('A')) dir.x -= 1;
        if(key_reporter('D')) dir.x += 1;
        if(key_reporter('R')) dir.z += 1;
        if(key_reporter('F')) dir.z -= 1;
    }
    return dir;
};

}

bool Camera_control::on_mouse_move_delta(double dx, double dy)
{
    if(!cursor_enabled()){
        rotate(dx,dy);
        return true;
    };
    return false;
}

//======== Point_watch ========//

Point_watch::Point_watch(Camera& cam):
    Camera_control (cam)
{
}

Point_watch::Point_watch(const Camera_control& cctrl):
    Camera_control (cctrl)
{
}

void Point_watch::update(float dt, const Key_status_fn& kf)
{
    auto& vpos  = camera().view_position();
    vpos.pos(generic_movement(*this,dt,get_generic_direction(kf)));
    vpos.direction(point_-vpos.pos());
}

//======== First_person_view ========//

First_person_view::First_person_view(Camera& cam):
    Camera_control (cam)
{
}

First_person_view::First_person_view(const Camera_control& cctrl):
    Camera_control (cctrl)
{
}

void First_person_view::update(float dt, const Key_status_fn& kf)
{
    auto& vpos  = camera().view_position();
    vpos.pos(generic_movement(*this,dt,get_generic_direction(kf)));
}

void First_person_view::rotate(float dx, float dy)
{
    using glm::vec3;
    auto& vp        = camera().view_position();
    auto rot        = vp.rot();
    float sensity   = 0.01;
    float roll      = glm::roll(rot)    - dx * sensity;
    float pitch     = glm::pitch(rot)   - dy * sensity;
    pitch           = glm::clamp<float>(pitch,-glm::pi<float>()*0.5f,glm::pi<float>()*0.5f);
    vp.rot(glm::angleAxis(roll,vec3(0,0,1)) * glm::angleAxis(pitch,vec3(1,0,0)));
}


} // namespace fgs_demo
