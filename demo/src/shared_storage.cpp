/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "shared_storage.hpp"


namespace fgs_demo {

void Shared_storage::init()
{
    balls_.storage().size_align(sizeof(Ball)*16);
    pls_.storage().size_align(sizeof(Point_light)*16);
}

void Shared_storage::upload_balls(const Scene_data::Balls& data)
{
    n_balls_ = data.size();
    balls_.storage().reallocate(sizeof(Ball)*n_balls_);
    balls_.storage().upload(0,data.data(),n_balls_);
}

void Shared_storage::upload_pl(const Scene_data::Point_lights& data)
{
    n_pls_ = data.size();
    pls_.storage().reallocate(sizeof(Point_light)*n_pls_);
    pls_.storage().upload(0,data.data(),n_pls_);
}

} // namespace fgs_demo
