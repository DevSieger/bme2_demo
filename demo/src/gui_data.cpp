/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include "gui_data.hpp"
#include "../3rd_party/stb_image.h"
#include <dk/logging.hpp>
#include <stdexcept>
#include <algorithm>

namespace fgs_demo {

Font_info::Font_info(Path path, Size grid, Char_size  char_size_p, Char_ix first_char_index):
    path_(std::move(path)),
    grid_size_(grid),
    char_size_(char_size_p),
    offset_(first_char_index)
{}

Text_label::Position Text_label::position(const Text_label::Origin_point& label_orig, const Position& window_size, const Text_label::Origin_point& window_orig) const
{
    using Orig      = Origin_point;

    auto lbl_size   = size();

    Position result;

    // gets the position of the lebel origin point relative to the bottom-left corner of the window
    switch ( window_origin().x ) {
        case Orig::LEFT     : { result.x    = position().x; };break;
        case Orig::CENTER   : { result.x    = window_size.x / 2 + position().x; };break;
        case Orig::RIGHT    : { result.x    = window_size.x     - position().x; };break;
    }

    switch ( window_origin().y ) {
        case Orig::BOTTOM   : { result.y    = position().y; };break;
        case Orig::MIDDLE   : { result.y    = window_size.y / 2 + position().y; };break;
        case Orig::TOP      : { result.y    = window_size.y     - position().y; };break;
    }

    // moves the result to the position of the given origin point
    if( origin().x != label_orig.x ){

        // moves result.x to the left most coordinate of the text label area from the x coordinate of the current origin point
        switch ( origin().x ) {
            case Orig::LEFT     : { };break;
            case Orig::CENTER   : { result.x   -= lbl_size.x / 2; };break;
            case Orig::RIGHT    : { result.x   -= lbl_size.x; };break;
        }

        // moves result.x to the x coordinate of the given origin point
        switch ( label_orig.x ) {
            case Orig::LEFT     : { };break;
            case Orig::CENTER   : { result.x   += lbl_size.x / 2; };break;
            case Orig::RIGHT    : { result.x   += lbl_size.x; };break;
        }
    }
    if( origin().y != label_orig.y ){
        // moves result.y to the bottom most coordinate of the text label area from the y coordinate of the current origin point
        switch ( origin().y ) {
            case Orig::BOTTOM   : { };break;
            case Orig::MIDDLE   : { result.y   -= lbl_size.y / 2; };break;
            case Orig::TOP      : { result.y   -= lbl_size.y; };break;
        }
        // moves result.y to the y coordinate of the given origin point
        switch ( label_orig.y ) {
            case Orig::BOTTOM   : {  };break;
            case Orig::MIDDLE   : { result.y   += lbl_size.y / 2; };break;
            case Orig::TOP      : { result.y   += lbl_size.y;};break;
        }
    }

    // makes the result to be relative to the given origin point of the window
    switch ( window_orig.x ) {
        case Orig::LEFT     : { };break;
        case Orig::CENTER   : { result.x   -= window_size.x / 2; };break;
        case Orig::RIGHT    : { result.x    = window_size.x - result.x; };break;
    }

    switch ( window_orig.y ) {
        case Orig::BOTTOM   : { };break;
        case Orig::MIDDLE   : { result.y   -= window_size.y / 2; };break;
        case Orig::TOP      : { result.y    = window_size.y - result.y; };break;
    }

    return result;
}

Text_label::Uint_size Text_label::text_size() const
{
    glm::uvec2 res(0,0);

    auto it         = text_.cbegin();
    const auto last = text_.cend();

    while(it!=last){
        auto next_line          = std::find(it,last,eol);

        std::size_t line_len    = next_line - it;
        if(res.x < line_len){
            res.x   = line_len;
        }
        ++res.y;

        it = next_line;
        if(it!=last){
            ++it;
        }
    }

    return res;
}

} // namespace fgs_demo
