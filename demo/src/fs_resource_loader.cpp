/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <cassert>
#include <dk/logging.hpp>
#include <fstream>
#include "fs_resource_loader.hpp"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#include "../3rd_party/stb_image.h"

#ifndef SHADERS_DIR
static_assert ("directory for shaders isn't specified");
#endif

#ifndef TEXTURES_DIR
static_assert ("directory for textures isn't specified");
#endif

namespace fs = std::filesystem;

namespace fgs_demo {

namespace {

class STBI_image_data: public Image_data {
public:
    STBI_image_data(Byte* data, Size w, Size h, Size c):
        Image_data(w,h,c), data_(data)
    {}

    Byte* get() const override {
        return data_;
    }

    ~STBI_image_data() override {
        stbi_image_free(data_);
    }

private:
    Byte* data_;
};

}

Fs_resource_loader::Fs_resource_loader(std::filesystem::path exec_path)
{
    // exec_path SHOULD be a path to the currently running executable.
    // if it isn't - this isn't a runtime error.
    assert(fs::is_regular_file(exec_path));

    fs::path dir    = canonical(exec_path.parent_path());

    if(dir.has_relative_path() && dir.filename() == "bin") {
        prefix_ = dir.parent_path();
    } else {
        dk::log(dk::Detail_level::CRITICAL, "the executable should be placed in \"bin\" directory,"
                                            " but it is placed in %1%. A fallback layout is used.", dir.filename() );
        prefix_= dir;
    }
}

Resource_loader::Shader_source Fs_resource_loader::load_shader_source(std::string_view shader_name) const
{
    std::string     result;
    fs::path        file_name   = prefix_ / SHADERS_DIR / shader_name;
    std::ifstream   file(file_name.c_str(), std::ios_base::binary);

    if(file.is_open()) {
        file.seekg(0, std::ios_base::end);
        auto file_size = file.tellg();

        if(file_size > 0){
            file.seekg(0, std::ios_base::beg);
            result.resize(static_cast<std::size_t>(file_size));
            file.read(&result[0], file_size);
        }

    } else {
        dk::log(dk::Detail_level::CRITICAL,"can't open file %1%", file_name);
    }

    return result;
}

Resource_loader::Image_ptr Fs_resource_loader::load_image(std::string_view image_name,unsigned desired_chnls) const
{
    fs::path file   = prefix_ / TEXTURES_DIR/ image_name;

    stbi_set_flip_vertically_on_load(1);
    int w, h, ch_in_file;
    Image_data::Byte* img = stbi_load(file.c_str(), &w, &h, &ch_in_file, static_cast<int>(desired_chnls));

    if(!img){
        dk::log(dk::Detail_level::NON_CRITICAL, "can't load image %1%: %2%", file, stbi_failure_reason());
        return Image_ptr{};
    }

    int channels = desired_chnls ? static_cast<int>(desired_chnls) : ch_in_file;
    return std::make_unique<STBI_image_data>(img, w, h, channels);
}

} // namespace fgs_demo
