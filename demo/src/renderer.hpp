/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/fgs.hpp>
#include "drawers.hpp"
#include "binding_points.hpp"
#include "shared_storage.hpp"
#include <string>

namespace fgs_demo {

struct Scene_data;
class Resource_loader;

class Renderer
{
public:
    Renderer() = default;
    Renderer(Renderer&&) = delete;

    void init(const Resource_loader& loader, unsigned width, unsigned height);
    void draw(const Scene_data& sc, float dt);

    void wire_mode(bool enabled){
        cfg_.wire_mod = enabled;
    }

    bool wire_mode() const {
        return cfg_.wire_mod;
    }

    enum class Ball_draw_mode{
        TESSELATION,
        RECONSTRUCTION
    };

    Ball_draw_mode ball_draw_mode()const{
        return cfg_.ball_mode;
    }
    void ball_draw_mode(Ball_draw_mode p_ball_draw_mode){
        cfg_.ball_mode = p_ball_draw_mode;
    }

    // the first is the section name, the second is the mean time in seconds :)
    // std::pair to break dependencies
    using Section_stat_entry = std::pair<std::string,double>;
    using Section_stat = std::vector<Section_stat_entry>;
    const Section_stat& get_stat() const;

    /// Tests if the performance counting is enabled
    bool    performance_counting() const {
        return cfg_.enable_perf_measure;
    }
    /// Sets the state of the performance counting
    void    performance_counting(bool enabled);

    void resize(unsigned width, unsigned height);

private:
    void init_fbs();
    void init_cs_tx();
    void init_shaders(const Resource_loader& rl);
    // sets uniforms that depends on the framebuffer size
    void shaders_on_resize();
    void init_textures(const Resource_loader& loader);

    void upload_data(const Scene_data& sc, float dt);
    void clear();

    enum class GB_stencil_mode{
        NONE,
        ENABLED,
        INVERTED,
        WRITE
    };

    void gbuffer_stencil_test(GB_stencil_mode mode);

    void geometry_pass();
    void light_stage();

    void calc_luminance_gmean();

    // actually may be few passes.
    // here should be tonemapping & antialiasing.
    void final_pass();

    struct{
        fgs::Shader_program balls_ts{"balls_ts"};
        fgs::Shader_program balls_rc{"balls_rc"};
        fgs::Shader_program ground{"ground"};
        fgs::Shader_program tonemap{"tonemap"};
        fgs::Shader_program ambient_dls{"ambient_dls"};
        fgs::Shader_program dl_intensity{"dls"};
        fgs::Shader_program dl_shdw{"dl_shdw"};
        fgs::Shader_program pl_shdw{"pl_shdw"};
        fgs::Shader_program pls{"pls"};
        fgs::Shader_program sky{"sky"};
        fgs::Shader_program luma_reduce{"luma_reduce"};
        fgs::Shader_program luma_reduce2{"luma_reduce"};
    } shdr_;

    fgs::Shader_program& ball_shader();

    Shared_storage      storage_;
    Full_screen_drawer  fsd_;
    Ball_drawer         ball_drwr_  {storage_};
    Point_light_drawer  pl_drwr_    {storage_};
    Dl_shadow_drawer    dl_sv_drwr_ {storage_};

    unsigned    width_;
    unsigned    height_;

    // we need to handle specially at least few frames after a scene switching or an initialization.
    unsigned                    frames_done_ = 0;
    // to prevent overflow.
    static constexpr unsigned   max_frames_to_count = std::numeric_limits<unsigned>::max();
    // to detect scene switch
    const Scene_data*                last_scene_ = nullptr;

    struct{
        fgs::Texture_2D albedo;
        fgs::Texture_2D spec;
        fgs::Texture_2D depth;
        fgs::Texture_2D hdr_color;
        fgs::Texture_2D normal;
        fgs::Texture_2D shadow_tmp_dl;
    } rt_;

    struct{
        // auxiliary texture to perform luminance reduction
        fgs::Texture_2D     luma_reduce_aux;
        // the geometric mean of luminance
        // "ping-pong" method is used.
        fgs::Texture_2D     luma_gmean_src[2];
        fgs::Texture_2D*    luma_gm_front   = {luma_gmean_src};
        fgs::Texture_2D*    luma_gm_back    = {luma_gmean_src+1};
    } luma_aux_;

    // the size of an area that is reduced to a single pixel after
    // a step of the luminance reduction
    glm::ivec2 get_reduce_step_size();

    struct{
        fgs::Texture_2D ground;
        fgs::Texture_2D ground_nm;
        fgs::Texture_2D ground_spec;
        fgs::Texture_2D ball_albedo;
    } tx_;

    fgs::Frame_buffer   gbuffer_;
    fgs::Frame_buffer   abuffer_;

    // holds temporary values of shadow for a directional light source
    fgs::Frame_buffer   fbo_shadow_dl_;

    struct Camera_data{
        glm::mat4x4 view;
        glm::mat4x4 proj;
        glm::mat4x4 inv_proj;
    };

    fgs::UBO<Camera_data> cam_ubo_{ubo_binding_points::CAMERA};

    struct Ambient_data{
                    Vec3    ambient_intensity;
        alignas(16) Vec3    sky_color;
    };

    fgs::SSBO<Ambient_data,Directional_light> dls_ssbo_{ssbo_binding_points::DIRECTIONAL_LS};

    struct Cfg_data{
        float adaptation_rate   = 0.04;
        float adaptation_min    = std::numeric_limits<float>::min();
        float adaptation_max    = std::numeric_limits<float>::max();
        float dt                = std::numeric_limits<float>::max();
        float burn_out_ratio    = 16;

        // values of automatic tonemapping key deduction
        float tm_key_a          = 0.19721736312181016487f;
        float tm_key_b          = 1;
        float tm_key_c          = 0;

        float pt_cut_off        = 0.025;
    };

    fgs::UBO<Cfg_data> sc_cfg_ubo_{ubo_binding_points::CFG};

    struct{
        bool wire_mod               = false;

        // ugly profiling
        bool enable_perf_measure    = true;
        double stat_update_interval = 1.0;
        Ball_draw_mode ball_mode    = Ball_draw_mode::RECONSTRUCTION;
    } cfg_;

    fgs::Performance_counter::Section section_start(unsigned ix);
    void update_stat(double dt);

    std::vector<fgs::Performance_counter> perf_counters_;

    struct{
        Section_stat    stat_;
        double          stat_elapsed_   = 0;  // elapsed time from the last update of the statistic
        glm::ivec2      reduction_size;
    } cache_;
};

} // namespace fgs_demo

