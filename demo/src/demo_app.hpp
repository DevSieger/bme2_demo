/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/sw_application.hpp>
#include <memory>
#include "gui.hpp"
#include "resource_loader.hpp"

namespace fgs_demo {

class Demo_app: public fgs::SW_application {
public:
    Demo_app(const Resource_loader& loader);
    Demo_app(Demo_app&&) = delete;
    ~Demo_app();
private:
    struct Data;

    void do_draw() override;
    void do_init(Size width, Size height) override;

    void on_key(  int key, int mods, Action act) override;
    void on_mouse_move(double x, double y) override;
    void on_resize(Size width, Size height) override;

    Data& data(){ return *m_;}
    const Data& data() const { return *m_;}

    Settings make_settings();
    std::string_view control_string() const;

    enum class Camera_type{
        LOOK_AT,
        FREE
    };
    // switches to the given camera control type
    void camera_type(Camera_type type);

    enum class Scene_ix{
        Test_scene,
        Benchmark,
        Snapshot,
        BME2
    };

    void switch_scene(Scene_ix sc);
    void switch_gui(GUI* gui);

    void rebuild_scene_settings();

    Font_ptr load_font(std::string_view path);

    // wraps "cursor_lock"
    // reason: helps track the difference in the cursor position
    void cursor_mode(bool locked);

    const Resource_loader&  loader_;
    std::unique_ptr<Data>   m_;
};

} // namespace fgs_demo
