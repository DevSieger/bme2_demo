# BME2

This demo is a remake of an application that I wrote as a part of my bachelor work, a simplified physics engine.
The main assumption of the work is object shape, the one with a very convenient form of inertia tensor: a ball.
The same shape makes possible few rendering tricks, which are implemented in the demo:

- an efficient way to render (almost) ray-casting quality shadows
- a "polygon-less" ball shape

### Windows binaries

https://bitbucket.org/DevSieger/bme2_demo/downloads/bme2_demo.zip
Binaries was compiled with a recent MinGW toolchain.
Note: I got reports that antivirus may break an application run.

## REQUIREMENTS

* GLM ( https://glm.g-truc.net )
* GLFW ( https://www.glfw.org )

You should install these libraries to make them available for cmake.

Tested on linux with gcc and clang.
MSVC compatibility was not tested.

#### HINT:

Press F1 to show controls.

Do not forget to use --recursive option to clone submodules as well, i.e.:

    git clone --recursive https://DevSieger@bitbucket.org/DevSieger/bme2_demo.git

